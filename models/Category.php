<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $title
 * @property int $depth
 * @property string $description
 * @property string $created_at
 *
 * @property ProductToCategory[] $productToCategories
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['depth'], 'integer'],
            [['depth'], 'default', 'value' => 1],
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'depth' => 'Depth',
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToCategories()
    {
        return $this->hasMany(ProductToCategory::class, ['category_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\CategoryQuery(get_called_class());
    }

    /**
     * @param array $ids
     * @return array
     */
    public static function getList(array $ids = []): array
    {
        $models = static::find();
        if (!empty($ids)) {
            $models->where(['id' => $ids]);
        }
        $models = $models->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    public function fields(): array
    {
        return [
            'id',
            'title',
            'description',
        ];
    }
}
