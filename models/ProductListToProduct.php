<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_list_to_product".
 *
 * @property int $product_list_id
 * @property int $product_id
 * @property int $quantity
 *
 * @property Product $product
 * @property ProductList $productList
 */
class ProductListToProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_list_to_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_list_id', 'product_id', 'quantity'], 'required'],
            [['product_list_id', 'product_id', 'quantity'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['product_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductList::className(), 'targetAttribute' => ['product_list_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_list_id' => 'Product List ID',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductList()
    {
        return $this->hasOne(ProductList::className(), ['id' => 'product_list_id']);
    }
}
