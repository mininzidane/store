<?php

namespace app\models;

/**
 * This is the model class for table "product_list".
 *
 * @property int $id
 * @property string $title
 * @property int $user_id
 * @property string $created_at
 *
 * @property User $user
 * @property Product[] $products
 * @property ProductListToProduct[] $productListToProducts
 */
class ProductList extends \yii\db\ActiveRecord
{
    public const DEFAULT_LIMIT = 50;

    public const STATUS_ACTIVE = 1;

    private $cartData = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['user_id', 'default', 'value' => \Yii::$app->user->id],
            [['title', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductListToProducts()
    {
        return $this->hasMany(ProductListToProduct::class, ['product_list_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\ProductListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ProductListQuery(static::class);
    }

    public function getProducts()
    {
        return $this
            ->hasMany(Product::class, ['id' => 'product_id'])
            ->via('productListToProducts');
    }

    /**
     * @param array $cartData format [$productId => $quantity, ...]
     * @return self
     */
    public function setCardData(array $cartData): self
    {
        $this->cartData = $cartData;
        return $this;
    }

    public function afterSave($insert, $changedAttributes): void
    {
        if (\count($this->cartData)) {
            $rows = [];
            foreach ($this->cartData as $productId => $quantity) {
                $rows[] = [$this->id, $productId, $quantity];
            }

            \Yii::$app->db
                ->createCommand()
                ->batchInsert(
                    ProductListToProduct::tableName(),
                    ['product_list_id', 'product_id', 'quantity'],
                    $rows
                )
                ->execute()
            ;
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function fields()
    {
        return [
            'id',
            'title',
            'products',
        ];
    }
}
