<?php

namespace app\models;

/**
 * This is the model class for table "order".
 *
 * @property int    $id
 * @property int    $user_id
 * @property string $address
 * @property string $comment
 * @property int    $status
 * @property string $created_at
 *
 * @property User $user
 * @property OrderToProduct[] $orderToProducts
 * @property Product[] $products
 */
class Order extends \yii\db\ActiveRecord
{
    public const DEFAULT_LIMIT = 50;

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

    private $cartData = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderToProducts()
    {
        return $this->hasMany(OrderToProduct::class, ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this
            ->hasMany(Product::class, ['id' => 'product_id'])
            ->via('orderToProducts');
    }


    /**
     * {@inheritdoc}
     * @return \app\models\queries\OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\OrderQuery(get_called_class());
    }

    /**
     * @param array $cartData format [$productId => $quantity, ...]
     * @return self
     */
    public function setCardData(array $cartData): self
    {
        $this->cartData = $cartData;
        return $this;
    }

    public function afterSave($insert, $changedAttributes): void
    {
        if (\count($this->cartData)) {
            $rows = [];
            foreach ($this->cartData as $productId => $quantity) {
                $rows[] = [$this->id, $productId, $quantity];
            }

            \Yii::$app->db
                ->createCommand()
                ->batchInsert(
                    OrderToProduct::tableName(),
                    ['order_id', 'product_id', 'quantity'],
                    $rows
                )
                ->execute()
            ;
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function fields(): array
    {
        return [
            'id',
            'products',
        ];
    }
}
