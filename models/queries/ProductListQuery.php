<?php

namespace app\models\queries;

use app\models\ProductList as Model;

/**
 * This is the ActiveQuery class for [[\app\models\ProductList]].
 *
 * @see \app\models\ProductList
 */
class ProductListQuery extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \app\models\ProductList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\ProductList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function limited(): self
    {
        return $this->limit(Model::DEFAULT_LIMIT);
    }

    public function active(): self
    {
        return $this; // todo until status column added
        return $this->andWhere('status = :status', [
            Model::tableName() . '.status' => Model::STATUS_ACTIVE
        ]);
    }

    public function own(): self
    {
        return $this->andWhere([
            Model::tableName() . '.user_id' => \Yii::$app->user->id
        ]);
    }
}
