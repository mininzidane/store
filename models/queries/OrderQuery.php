<?php

namespace app\models\queries;

use app\models\Order;

/**
 * This is the ActiveQuery class for [[\app\models\Order]].
 *
 * @see \app\models\Order
 */
class OrderQuery extends \yii\db\ActiveQuery
{
    public function active(): self
    {
        return $this->andWhere('order.status = :status', [
            ':status' => Order::STATUS_ACTIVE
        ]);
    }

    public function own(): self
    {
        return $this->andWhere([
            'order.user_id' => \Yii::$app->user->id
        ]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Order[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Order|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function limited(): self
    {
        return $this->limit(Order::DEFAULT_LIMIT);
    }
}
