<?php

namespace app\models\queries;

use app\models\User;

/**
 * This is the ActiveQuery class for [[\app\models\User]].
 *
 * @see \app\models\User
 */
class UserQuery extends \yii\db\ActiveQuery
{
    public function active(): self
    {
        return $this->andWhere('user.status = :status', [
            ':status' => User::STATUS_ACTIVE,
        ]);
    }

    public function limited(): self
    {
        return $this->limit(User::DEFAULT_LIMIT);
    }
}
