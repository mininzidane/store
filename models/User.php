<?php

namespace app\models;

use app\models\queries\UserQuery;
use yii\helpers\ArrayHelper;

/**
 * Class User
 * @package app\models
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $address
 * @property int    $status
 * @property string $access_token
 * @property string $created_at
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public const DEFAULT_LIMIT = 50;

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

    public $authKey;

    public static function tableName(): string
    {
        return 'user';
    }

    public static function find()
    {
        return new UserQuery(static::class);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        if (YII_DEBUG) {
            return self::find()->one();
        }
        return self::find()->where(['access_token' => $token])->one();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['username' => $username])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey(): string
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey): bool
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password): bool
    {
        try {
            $success = \Yii::$app->security->validatePassword($password, $this->password);
        } catch (\Exception $e) {
            return false;
        }
        return $success;
    }


    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        if ($password) {
            $this->password = \Yii::$app->security->generatePasswordHash($password);
        } elseif (!$this->isNewRecord) {
            $this->password = $this->getOldAttribute('password');
        }
    }

    public static function getList()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'username');
    }

    public function beforeSave($insert): bool
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->setPassword($this->password);
        return true;
    }

    public function rules(): array
    {
        return [
            [['username', 'password', 'access_token'], 'required'],
            [['username', 'access_token'], 'unique'],
        ];
    }
}
