<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "product".
 *
 * @property int                 $id
 * @property string              $title
 * @property string              $description
 * @property string              $created_at
 *
 * @property OrderToProduct[]    $orderToProducts
 * @property ProductToCategory[] $productToCategories
 * @property Category[] $categories
 * @property ProductPrice[] $prices
 */
class Product extends \yii\db\ActiveRecord
{
    public const DEFAULT_LIMIT = 50;

    public $categoryIds = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['categoryIds', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderToProducts()
    {
        return $this->hasMany(OrderToProduct::class, ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToCategories()
    {
        return $this->hasMany(ProductToCategory::class, ['product_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ProductQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this
            ->hasMany(Category::class, ['id' => 'category_id'])
            ->via('productToCategories');
    }

    public function afterFind()
    {
        foreach ($this->categories as $category) {
            $this->categoryIds[] = $category->id;
        }
        parent::afterFind();
    }

    public function afterSave($insert, $changedAttributes)
    {
        $rows = [];
        foreach ($this->categoryIds as $categoryId) {
            $rows[] = [$this->id, $categoryId];
        }
        if (!empty($rows)) {
            \Yii::$app->db
                ->createCommand()
                ->batchInsert(
                    ProductToCategory::tableName(),
                    ['product_id', 'category_id'],
                    $rows
                )
                ->execute();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function fields()
    {
        return [
            'id',
            'title',
            'description',
            'price',
        ];
    }

    public function getPrices(): ActiveQuery
    {
        return $this->hasMany(ProductPrice::class, [
            'product_id' => 'id'
        ]);
    }

    public function getPrice(): ?string
    {
        return array_key_exists(0, $this->prices) ? number_format($this->prices[0]->price, 2) : null;
    }
}
