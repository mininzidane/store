<?php

use yii\db\Migration;

/**
 * Class m180918_090934_init_tables
 */
class m180918_090934_init_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable('product_to_category', [
            'product_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);
        $this->createIndex('ix_product_id_category_id', 'product_to_category', ['product_id', 'category_id']);

        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'depth' => $this->integer(3)->notNull()->defaultValue(0),
            'description' => $this->text(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createTable('order_to_product', [
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
        ]);
        $this->createIndex('ix_order_id_product_id', 'order_to_product', ['order_id', 'product_id']);

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'access_token' => $this->string(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
        $this->insert(
            'user',
            [
                'username' => 'admin',
                'password' => \Yii::$app->security->generatePasswordHash('qwerty123456'),
                'access_token' => \Yii::$app->security->generateRandomString(),
            ]
        );

        $this->addForeignKey(
            'fk_product_to_category_product_id',
            'product_to_category',
            'product_id',
            'product',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_product_to_category_category_id',
            'product_to_category',
            'category_id',
            'category',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_order_user_id',
            'order',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_order_to_product_product_id',
            'order_to_product',
            'product_id',
            'product',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_order_to_product_order_id',
            'order_to_product',
            'order_id',
            'order',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_order_to_product_order_id', 'order_to_product');
        $this->dropForeignKey('fk_order_to_product_product_id', 'order_to_product');
        $this->dropForeignKey('fk_order_user_id', 'order');
        $this->dropForeignKey('fk_product_to_category_category_id', 'product_to_category');
        $this->dropForeignKey('fk_product_to_category_product_id', 'product_to_category');

        $this->dropTable('user');
        $this->dropTable('order_to_product');
        $this->dropTable('order');
        $this->dropTable('category');
        $this->dropTable('product_to_category');
        $this->dropTable('product');
    }
}
