<?php

use yii\db\Migration;

/**
 * Class m181101_071010_add_new_fields_to_order_and_user_tables
 */
class m181101_071010_add_new_fields_to_order_and_user_tables extends Migration
{
    private const TABLE_ORDER = 'order';
    private const TABLE_USER = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_ORDER, 'address', $this->string()->after('user_id'));
        $this->addColumn(self::TABLE_ORDER, 'comment', $this->text()->after('address'));
        $this->addColumn(self::TABLE_ORDER, 'status', $this->integer(1)->defaultValue(1)->after('comment'));
        $this->addColumn(self::TABLE_USER, 'address', $this->string()->after('password'));
        $this->addColumn(self::TABLE_USER, 'status', $this->integer(1)->defaultValue(1)->after('address'));
        $this->alterColumn('product_price', 'price', $this->decimal(9, 2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('product_price', 'price', $this->integer());
        $this->dropColumn(self::TABLE_USER, 'status');
        $this->dropColumn(self::TABLE_USER, 'address');
        $this->dropColumn(self::TABLE_ORDER, 'status');
        $this->dropColumn(self::TABLE_ORDER, 'comment');
        $this->dropColumn(self::TABLE_ORDER, 'address');
    }
}
