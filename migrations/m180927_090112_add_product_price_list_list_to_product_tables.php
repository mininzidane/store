<?php

use yii\db\Migration;

/**
 * Class m180927_090112_add_product_price_product_list_product_list_to_product_tables
 */
class m180927_090112_add_product_price_list_list_to_product_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_price', [
            'id' => $this->primaryKey(),
            'price' => $this->integer(),
            'product_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
        $this->createTable('product_list', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
        $this->createTable('product_list_to_product', [
            'product_list_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
        ]);
        $this->createIndex('ix_product_list_id_product_id', 'product_list_to_product', ['product_list_id', 'product_id']);
        $this->addColumn('order_to_product', 'quantity', $this->integer()->notNull());

        $this->addForeignKey(
            'fk_product_price_product_id',
            'product_price',
            'product_id',
            'product',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_product_list_user_id',
            'product_list',
            'user_id',
            'user',
            'id'
            ,
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_product_list_to_product_product_list_id',
            'product_list_to_product',
            'product_list_id',
            'product_list',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_product_list_to_product_product_id',
            'product_list_to_product',
            'product_id',
            'product',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_product_list_to_product_product_id', 'product_list_to_product');
        $this->dropForeignKey('fk_product_list_to_product_product_list_id', 'product_list_to_product');
        $this->dropForeignKey('fk_product_list_user_id', 'product_list');
        $this->dropForeignKey('fk_product_price_product_id', 'product_price');
        $this->dropColumn('order_to_product', 'quantity');
        $this->dropTable('product_list_to_product');
        $this->dropTable('product_list');
        $this->dropTable('product_price');
    }
}
