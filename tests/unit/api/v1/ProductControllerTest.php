<?php

namespace tests\api\v1;

use app\modules\v1\controllers\ProductController;
use yii\helpers\ArrayHelper;

/**
 * Class ProductControllerTest
 * @package tests\api\v1
 * @method ProductController getController()
 */
class ProductControllerTest extends BaseTest
{
    protected const CONTROLLER_CLASS = ProductController::class;

    /**
     * @dataProvider searchProvider
     * @param array $categoryIds
     * @param array $productIds
     */
    public function testSearchByCategories(array $categoryIds, array $productIds): void
    {
        $this->useCredentials();
        $controller = $this->getController();
        $result = $controller->actionSearchByCategories(implode(',', $categoryIds));
        $this->assertArraySubset($productIds, ArrayHelper::getColumn($result['data']['products'], 'id'));
    }

    public function searchProvider(): array
    {
        return [
            [[1, 5], [8, 66, 85]],
            [[2, 4], [84, 81, 74]],
            [[10], [89, 21, 44, 78, 9]],
        ];
    }
}
