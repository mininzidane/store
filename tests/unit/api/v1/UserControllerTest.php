<?php

namespace tests\api\v1;

use app\models\User;
use app\modules\v1\controllers\UserController;

class UserControllerTest extends BaseTest
{
    protected const CONTROLLER_CLASS = UserController::class;

    public function testRegistrationAndLogin(): void
    {
        $params = [
            'username' => 'test123',
            'password' => 'test123'
        ];
        \Yii::$app->request->setBodyParams($params);
        /** @var UserController $controller */
        $controller = $this->getController();
        $result = $controller->actionCreate();
        $this->assertEquals(self::RESPONSE_STATUS_OK, $result['status']);
        $token = $result['data'];
        $user = User::findOne(['access_token' => $token]);
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($params['username'], $user->username);

        $result = $controller->actionLogin();
        $this->assertEquals(self::RESPONSE_STATUS_OK, $result['status']);
        /** @var User $user */
        $user = \Yii::$app->user->identity;
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($params['username'], $user->username);
    }
}
