<?php

namespace tests\api\v1;

use app\modules\v1\controllers\CategoryController;
use yii\helpers\ArrayHelper;

class CategoryControllerTest extends BaseTest
{
    protected const CONTROLLER_CLASS = CategoryController::class;

    /**
     * @dataProvider searchProvider
     * @param string $query
     * @param array  $ids
     */
    public function testSearch(string $query, array $ids): void
    {
        $this->useCredentials();
        /** @var CategoryController $controller */
        $controller = $this->getController();
        $result = $controller->actionSearch($query);
        $this->assertArraySubset($ids, ArrayHelper::getColumn($result['data']['categories'], 'id'));
    }

    public function searchProvider(): array
    {
        return [
            ['alias', [1, 94]],
            ['vero', [7]],
            ['uptas', [1, 11, 19, 21, 65, 86]],
        ];
    }
}
