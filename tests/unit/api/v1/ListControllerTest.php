<?php

namespace tests\api\v1;

use app\models\ProductList;
use app\modules\v1\controllers\ListController;
use yii\helpers\ArrayHelper;

/**
 * Class ListControllerTest
 * @package tests\api\v1
 * @method ListController getController()
 */
class ListControllerTest extends BaseTest
{
    protected const CONTROLLER_CLASS = ListController::class;

    public function testCreate(): void
    {
        $this->useCredentials();
        $params = [
            'product_ids' => [
                1 => 3,
                7 => 2,
                9 => 5,
            ],
            'title' => 'super test',
        ];
        \Yii::$app->request->setBodyParams($params);
        $controller = $this->getController();
        $result = $controller->actionCreate();
        $listId = $result['data']['list']['id'];
        $productIds = ArrayHelper::getColumn($result['data']['list']['products'], 'product.id');
        $list = ProductList::findOne($listId);
        $listProductIds = ArrayHelper::getColumn($list->products, 'id');
        $this->assertArraySubset($productIds, $listProductIds);
        $this->assertEquals($params['title'], $list->title);
    }

    public function testUpdate(): void
    {
        $this->useCredentials();
        $params = [
            'product_ids' => [
                3 => 5,
                16 => 10,
                22 => 88,
            ],
            'title' => 'another super test',
        ];
        \Yii::$app->request->setBodyParams($params);
        $controller = $this->getController();
        $listId = 5;
        $result = $controller->actionUpdate($listId);
        $this->assertEquals($result['data']['list']['id'], $listId);
        $this->assertEquals($result['data']['list']['title'], $params['title']);
        foreach ($result['data']['list']['products'] as $productData) {
            $this->assertEquals($params['product_ids'][$productData['product']['id']], $productData['quantity']);
            $this->checkProductDataElement($productData['product']);
        }
    }

    public function testList(): void
    {
        $this->useCredentials();
        $controller = $this->getController();
        $result = $controller->actionList();
        foreach (\array_slice($result['data']['productLists'], 0, 10) as $productList) {
            $this->assertInternalType('integer', $productList['id']);
            $this->assertInternalType('string', $productList['title']);
            foreach ($productList['products'] as $product) {
                $this->checkProductDataElement($product);
            }
        }
    }

    public function testDelete(): void
    {
        $this->useCredentials();
        $controller = $this->getController();
        $id = 14;
        $result = $controller->actionDelete($id);
        $this->assertArraySubset(['status' => 'ok'], $result);
        $productList = ProductList::findOne($id);
        $this->assertNull($productList);
    }
}
