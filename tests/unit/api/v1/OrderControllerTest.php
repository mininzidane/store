<?php

namespace tests\api\v1;

use app\models\Order;
use app\models\Product;
use app\modules\v1\controllers\OrderController;

/**
 * Class OrderControllerTest
 * @package tests\api\v1
 * @method OrderController getController()
 */
class OrderControllerTest extends BaseTest
{
    protected const CONTROLLER_CLASS = OrderController::class;

    public function testPreCreate(): void
    {
        $this->useCredentials();
        $params = [
            'product_ids' => [
                1 => 5,
                2 => 8,
                6 => 1,
                13 => 2,
            ],
            'list_ids' => [1, 2],
        ];
        \Yii::$app->request->setBodyParams($params);
        /** @var OrderController $controller */
        $controller = $this->getController();
        $result = $controller->actionPreCreate();
        $expectedResult = [
            [
                'quantity' => 5,
                'product' =>
                    [
                        'id' => 1,
                        'title' => 'nam adipisci',
                        'description' => 'Nesciunt explicabo aut et.',
                        'price' => '103,743.06',
                    ],
            ],
            [
                'quantity' => 8,
                'product' =>
                    [
                        'id' => 2,
                        'title' => 'totam molestias',
                        'description' => 'Autem eaque dolore possimus.',
                        'price' => '864,980.35',
                    ],
            ],
            [
                'quantity' => 1,
                'product' =>
                    [
                        'id' => 6,
                        'title' => 'aut nobis',
                        'description' => 'Quisquam occaecati earum perferendis quaerat.',
                        'price' => '913,372.30',
                    ],
            ],
            [
                'quantity' => 4,
                'product' =>
                    [
                        'id' => 11,
                        'title' => 'autem dolorem',
                        'description' => 'Cupiditate fugiat voluptatem molestias non.',
                        'price' => '295,646.30',
                    ],
            ],
            [
                'quantity' => 2,
                'product' =>
                    [
                        'id' => 13,
                        'title' => 'eos quas',
                        'description' => 'Laboriosam blanditiis ullam facere ea dolorum.',
                        'price' => '819,747.96',
                    ],
            ],
            [
                'quantity' => 1,
                'product' =>
                    [
                        'id' => 14,
                        'title' => 'occaecati aut',
                        'description' => 'Nesciunt est dolorem corporis dolor minus.',
                        'price' => '531,633.39',
                    ],
            ],
            [
                'quantity' => 9,
                'product' =>
                    [
                        'id' => 15,
                        'title' => 'delectus voluptas',
                        'description' => 'Vel voluptas aliquid adipisci.',
                        'price' => '5,173.97',
                    ],
            ],
            [
                'quantity' => 8,
                'product' =>
                    [
                        'id' => 24,
                        'title' => 'expedita ad',
                        'description' => 'Inventore omnis expedita ab quia aut aliquid.',
                        'price' => '451,740.93',
                    ],
            ],
            [
                'quantity' => 7,
                'product' =>
                    [
                        'id' => 59,
                        'title' => 'assumenda fugiat',
                        'description' => 'Ipsam libero nulla minima officiis enim.',
                        'price' => '489,564.47',
                    ],
            ],
            [
                'quantity' => 7,
                'product' =>
                    [
                        'id' => 64,
                        'title' => 'placeat perspiciatis',
                        'description' => 'Molestiae asperiores totam numquam quis ut.',
                        'price' => '441,754.06',
                    ],
            ],
            [
                'quantity' => 4,
                'product' =>
                    [
                        'id' => 87,
                        'title' => 'deleniti voluptates',
                        'description' => 'Earum nisi qui error eum.',
                        'price' => '190,163.62',
                    ],
            ],
        ];
        $this->assertCount(11, $expectedResult);
        $this->assertArraySubset($expectedResult, $result['data']['products']);
    }

    public function testCreate(): void
    {
        $this->useCredentials();
        $params = [
            'product_ids' => [
                2 => 10,
                5 => 20,
                10 => 2,
            ],
        ];
        \Yii::$app->request->setBodyParams($params);
        $controller = $this->getController();
        $result = $controller->actionCreate();
        $this->assertInternalType('integer', $result['data']['order']['id']);
        foreach ($result['data']['order']['products'] as $productData) {
            $this->assertEquals($params['product_ids'][$productData['product']['id']], $productData['quantity']);
            $product = Product::findOne($productData['product']['id']);
            $this->assertEquals($productData['product']['title'], $product->title);
            $this->assertEquals($productData['product']['description'], $product->description);
            $this->assertEquals($productData['product']['price'], $product->getPrice());
        }
    }

    public function testList(): void
    {
        $this->useCredentials();
        $controller = $this->getController();
        $result = $controller->actionList();
        $this->assertCount(50, $result['data']['orders']);
        foreach (\array_slice($result['data']['orders'], 0, 10) as $order) {
            $this->assertInternalType('integer', $order['id']);
            foreach ($order['products'] as $product) {
                $this->checkProductDataElement($product);
            }
        }
    }

    public function testDelete(): void
    {
        $this->useCredentials();
        $controller = $this->getController();
        $id = 10;
        $result = $controller->actionDelete($id);
        $this->assertArraySubset(['status' => 'ok'], $result);
        $order = Order::findOne($id);
        $this->assertEquals(Order::STATUS_INACTIVE, $order->status);
    }
}
