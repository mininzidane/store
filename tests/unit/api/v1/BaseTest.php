<?php

namespace tests\api\v1;

use app\models\Product;
use app\models\User;
use Codeception\TestCase\Test;
use PHPUnit\Framework\TestCase;
use yii\base\Module;
use yii\db\Transaction;

/**
 * Class BaseTest
 * @package tests\api\v1
 *
 * Use `codecept run tests/unit` command
 */
abstract class BaseTest extends \Codeception\Test\Unit
{
    protected const CONTROLLER_CLASS = '';
    protected const CONTROLLER_ID = 'v1';

    // values from \app\modules\v1\Module constants
    protected const RESPONSE_STATUS_OK = 'ok';
    protected const RESPONSE_STATUS_NOT_OK = 'error';

    /** @var Transaction */
    protected $transaction;

    protected function setUp()
    {
        parent::setUp();
        $this->transaction = \Yii::$app->db->beginTransaction();
    }

    protected function tearDown()
    {
        $this->transaction->rollBack();
        parent::tearDown();
    }

    protected function useCredentials(): self
    {
        $user = User::findOne(['id' => 1]);
        \Yii::$app->request->headers->add('Autorization', "Bearer {$user->access_token}");
        \Yii::$app->user->setIdentity($user);
        return $this;
    }

    public function getController()
    {
        $controllerType = 'app';
        $module = new Module("app-{$controllerType}");
        $controllerClass = static::CONTROLLER_CLASS;
        $controllerId = static::CONTROLLER_ID;

        $rootPath = \Yii::getAlias("@{$controllerType}");

        $module->setBasePath($rootPath);

//        $module->layout = "main.php";

        $controller = new $controllerClass($controllerId, $module);
        \Yii::$app->controller = $controller;

        return $controller;
    }

    public function checkProductDataElement($productData): void
    {
        $product = Product::findOne($productData['id']);
        $this->assertEquals($productData['title'], $product->title);
        $this->assertEquals($productData['description'], $product->description);
        $this->assertEquals($productData['price'], $product->getPrice());
    }
}
