<?php

namespace tests\fixtures;

use app\models\ProductToCategory;
use yii\test\ActiveFixture;

class ProductToCategoryFixture extends ActiveFixture
{
    public $modelClass = ProductToCategory::class;
    public $depends = [ProductFixture::class, CategoryFixture::class];
}
