<?php

namespace tests\fixtures;

use app\models\ProductList;
use yii\test\ActiveFixture;

class ProductListFixture extends ActiveFixture
{
    public $modelClass = ProductList::class;
}
