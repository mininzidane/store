<?php

namespace tests\fixtures;

use app\models\Product;
use yii\test\ActiveFixture;

class ProductFixture extends ActiveFixture
{
    public $modelClass = Product::class;
}
