<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

ini_set('memory_limit', '-1');

// count = 100
return [
    'title' => implode(' ', $faker->words(2)),
    'description' => $faker->text(50),
];