<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

ini_set('memory_limit', '-1');

// count = 200
return [
    'product_id' => $faker->randomElement(range(1, 100)),
    'category_id' => $faker->randomElement(range(1, 100)),
];