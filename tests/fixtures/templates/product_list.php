<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

ini_set('memory_limit', '-1');

// count = 30
return [
    'title' => implode(' ', $faker->words(2)),
    'user_id' => 1,
];