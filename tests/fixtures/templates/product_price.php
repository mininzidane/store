<?php

/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

ini_set('memory_limit', '-1');

// count = 100
return [
    'price' => $faker->randomFloat(3, 0, 999999),
    'product_id' => $index + 1,
];