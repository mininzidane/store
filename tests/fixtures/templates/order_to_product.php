<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

ini_set('memory_limit', '-1');

// count = 200
return [
    'order_id' => $faker->randomElement(range(1, 100)),
    'product_id' => $faker->randomElement(range(1, 100)),
    'quantity' => $faker->randomElement(range(1, 10)),
];