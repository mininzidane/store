<?php

$faker = Faker\Factory::create();

ini_set('memory_limit', '-1');

// count = 100
return [
    'user_id' => 1,
    'address' => $faker->address,
    'comment' => $faker->text(),
];