<?php

namespace tests\fixtures;

use app\models\ProductPrice;
use yii\test\ActiveFixture;

class ProductPriceFixture extends ActiveFixture
{
    public $modelClass = ProductPrice::class;
    public $depends = [ProductFixture::class];
}
