<?php

namespace tests\fixtures;

use app\models\ProductListToProduct;
use yii\test\ActiveFixture;

class ProductListToProductFixture extends ActiveFixture
{
    public $modelClass = ProductListToProduct::class;
    public $depends = [ProductListFixture::class, ProductFixture::class];
}
