<?php

namespace tests\fixtures;

use app\models\OrderToProduct;
use yii\test\ActiveFixture;

class OrderToProductFixture extends ActiveFixture
{
    public $modelClass = OrderToProduct::class;
    public $depends = [ProductFixture::class, OrderFixture::class];
}
