<?php

namespace tests\fixtures;

use app\models\Order;
use yii\test\ActiveFixture;

class OrderFixture extends ActiveFixture
{
    public $modelClass = Order::class;
}
