<?php

return [
    'category0' => [
        'title' => 'voluptas alias',
        'depth' => 1,
        'description' => 'Maxime laborum et laborum aut excepturi.',
    ],
    'category1' => [
        'title' => 'sunt aperiam',
        'depth' => 2,
        'description' => 'Eveniet accusamus cupiditate incidunt magnam.',
    ],
    'category2' => [
        'title' => 'doloribus officia',
        'depth' => 3,
        'description' => 'Explicabo quia rerum neque totam est sapiente.',
    ],
    'category3' => [
        'title' => 'exercitationem optio',
        'depth' => 2,
        'description' => 'Et quam eum nesciunt.',
    ],
    'category4' => [
        'title' => 'modi incidunt',
        'depth' => 2,
        'description' => 'Laudantium unde provident praesentium.',
    ],
    'category5' => [
        'title' => 'quos qui',
        'depth' => 3,
        'description' => 'Consequatur vero quas sed aut.',
    ],
    'category6' => [
        'title' => 'vero cum',
        'depth' => 2,
        'description' => 'Distinctio eveniet aperiam eos eum.',
    ],
    'category7' => [
        'title' => 'ratione magni',
        'depth' => 2,
        'description' => 'Sapiente sit nulla optio maiores nihil.',
    ],
    'category8' => [
        'title' => 'dignissimos ut',
        'depth' => 3,
        'description' => 'Magni itaque soluta rem dolor aut aut aut.',
    ],
    'category9' => [
        'title' => 'adipisci eveniet',
        'depth' => 3,
        'description' => 'Rerum consequatur repellat rerum nemo rerum est.',
    ],
    'category10' => [
        'title' => 'voluptas reprehenderit',
        'depth' => 2,
        'description' => 'Magni sint voluptas praesentium necessitatibus.',
    ],
    'category11' => [
        'title' => 'cumque veniam',
        'depth' => 1,
        'description' => 'Aut autem et quia aut et accusantium.',
    ],
    'category12' => [
        'title' => 'est quos',
        'depth' => 2,
        'description' => 'Est fugit possimus nam eligendi expedita.',
    ],
    'category13' => [
        'title' => 'fugit repellendus',
        'depth' => 1,
        'description' => 'Amet perspiciatis ut amet amet qui ipsa est.',
    ],
    'category14' => [
        'title' => 'ut quisquam',
        'depth' => 1,
        'description' => 'Veniam a eaque est eaque atque ut vel qui.',
    ],
    'category15' => [
        'title' => 'veniam aut',
        'depth' => 1,
        'description' => 'Autem sit non ducimus laborum.',
    ],
    'category16' => [
        'title' => 'qui quis',
        'depth' => 1,
        'description' => 'Et consequatur illum cumque saepe cum sed.',
    ],
    'category17' => [
        'title' => 'aliquam qui',
        'depth' => 2,
        'description' => 'Voluptatem laboriosam neque dolorem.',
    ],
    'category18' => [
        'title' => 'voluptas sed',
        'depth' => 3,
        'description' => 'In placeat labore minima nihil facere ullam vel.',
    ],
    'category19' => [
        'title' => 'iusto ipsum',
        'depth' => 2,
        'description' => 'Saepe voluptas dolorem eius aut.',
    ],
    'category20' => [
        'title' => 'dicta voluptas',
        'depth' => 2,
        'description' => 'Rerum earum maiores aut velit.',
    ],
    'category21' => [
        'title' => 'porro rerum',
        'depth' => 3,
        'description' => 'Natus accusamus quod vero quis ex voluptate qui.',
    ],
    'category22' => [
        'title' => 'sed dolorem',
        'depth' => 2,
        'description' => 'Et tempora nihil voluptate enim.',
    ],
    'category23' => [
        'title' => 'nihil quos',
        'depth' => 1,
        'description' => 'In inventore aut ut assumenda non.',
    ],
    'category24' => [
        'title' => 'qui dolores',
        'depth' => 1,
        'description' => 'Sed dolor culpa est quas illo.',
    ],
    'category25' => [
        'title' => 'suscipit voluptatem',
        'depth' => 1,
        'description' => 'Quaerat molestiae error eum.',
    ],
    'category26' => [
        'title' => 'earum quia',
        'depth' => 1,
        'description' => 'Qui omnis non libero.',
    ],
    'category27' => [
        'title' => 'aliquam officiis',
        'depth' => 2,
        'description' => 'Officiis et cum est voluptatem.',
    ],
    'category28' => [
        'title' => 'corrupti numquam',
        'depth' => 1,
        'description' => 'Consectetur iste officia delectus repellat.',
    ],
    'category29' => [
        'title' => 'a quidem',
        'depth' => 1,
        'description' => 'Aspernatur facilis eligendi nihil aut nobis.',
    ],
    'category30' => [
        'title' => 'sint impedit',
        'depth' => 3,
        'description' => 'Rerum enim nihil ipsam aut enim suscipit ut.',
    ],
    'category31' => [
        'title' => 'et quidem',
        'depth' => 3,
        'description' => 'Non accusantium exercitationem amet.',
    ],
    'category32' => [
        'title' => 'dolorum ad',
        'depth' => 2,
        'description' => 'Sint qui eveniet omnis atque nesciunt eum.',
    ],
    'category33' => [
        'title' => 'veniam natus',
        'depth' => 3,
        'description' => 'Laboriosam cum et amet voluptas.',
    ],
    'category34' => [
        'title' => 'aliquam tempora',
        'depth' => 2,
        'description' => 'Incidunt sunt libero sed fugiat qui.',
    ],
    'category35' => [
        'title' => 'asperiores voluptates',
        'depth' => 1,
        'description' => 'Et sit illo sequi asperiores quae.',
    ],
    'category36' => [
        'title' => 'quod explicabo',
        'depth' => 1,
        'description' => 'Ipsam dolores ab dolores rem repellendus.',
    ],
    'category37' => [
        'title' => 'et repellat',
        'depth' => 1,
        'description' => 'Mollitia est et illo quisquam autem.',
    ],
    'category38' => [
        'title' => 'delectus eum',
        'depth' => 2,
        'description' => 'Omnis beatae dolores et ut vitae in quia.',
    ],
    'category39' => [
        'title' => 'doloribus in',
        'depth' => 2,
        'description' => 'Quam quo commodi et quaerat aliquid ut enim.',
    ],
    'category40' => [
        'title' => 'et tempore',
        'depth' => 2,
        'description' => 'Autem qui pariatur earum.',
    ],
    'category41' => [
        'title' => 'sit omnis',
        'depth' => 1,
        'description' => 'Eos itaque id quis assumenda.',
    ],
    'category42' => [
        'title' => 'quo odit',
        'depth' => 2,
        'description' => 'Eligendi amet voluptatem ut asperiores.',
    ],
    'category43' => [
        'title' => 'quia soluta',
        'depth' => 3,
        'description' => 'Porro in consequatur eum perferendis.',
    ],
    'category44' => [
        'title' => 'cum doloremque',
        'depth' => 3,
        'description' => 'Voluptatem et et similique quia sunt quod.',
    ],
    'category45' => [
        'title' => 'eos aliquam',
        'depth' => 2,
        'description' => 'Eveniet consequatur odio quia perspiciatis sit.',
    ],
    'category46' => [
        'title' => 'impedit dolorum',
        'depth' => 1,
        'description' => 'Laboriosam recusandae nobis ex.',
    ],
    'category47' => [
        'title' => 'iure sint',
        'depth' => 2,
        'description' => 'Ipsam tenetur quis ea modi ut et.',
    ],
    'category48' => [
        'title' => 'qui sed',
        'depth' => 2,
        'description' => 'Voluptatem non ipsum quis accusamus.',
    ],
    'category49' => [
        'title' => 'consequuntur excepturi',
        'depth' => 1,
        'description' => 'Commodi quia fugit dolorem expedita.',
    ],
    'category50' => [
        'title' => 'id et',
        'depth' => 1,
        'description' => 'At consequatur veniam est non non qui labore.',
    ],
    'category51' => [
        'title' => 'nisi dolores',
        'depth' => 1,
        'description' => 'Ut dolorum enim et iure et.',
    ],
    'category52' => [
        'title' => 'numquam ratione',
        'depth' => 2,
        'description' => 'In quos nemo et atque in enim.',
    ],
    'category53' => [
        'title' => 'quia omnis',
        'depth' => 3,
        'description' => 'Et provident molestias est dolorum nihil quo.',
    ],
    'category54' => [
        'title' => 'molestiae nam',
        'depth' => 2,
        'description' => 'Vitae totam animi ipsa error libero ab.',
    ],
    'category55' => [
        'title' => 'error cum',
        'depth' => 2,
        'description' => 'Tempora et dicta exercitationem.',
    ],
    'category56' => [
        'title' => 'et ipsa',
        'depth' => 1,
        'description' => 'Soluta dicta ut sapiente minima.',
    ],
    'category57' => [
        'title' => 'quo illo',
        'depth' => 3,
        'description' => 'Omnis doloribus veritatis in optio et sed.',
    ],
    'category58' => [
        'title' => 'et aperiam',
        'depth' => 2,
        'description' => 'Odio quos alias quisquam.',
    ],
    'category59' => [
        'title' => 'delectus omnis',
        'depth' => 2,
        'description' => 'Non voluptas quam quia totam ut.',
    ],
    'category60' => [
        'title' => 'accusamus debitis',
        'depth' => 3,
        'description' => 'Fugiat consequatur et et esse dignissimos.',
    ],
    'category61' => [
        'title' => 'at ipsum',
        'depth' => 3,
        'description' => 'Dolore quia in ea in quae voluptas et laborum.',
    ],
    'category62' => [
        'title' => 'eum at',
        'depth' => 1,
        'description' => 'Mollitia occaecati occaecati velit non.',
    ],
    'category63' => [
        'title' => 'voluptatibus nihil',
        'depth' => 2,
        'description' => 'Optio ut qui minus vel.',
    ],
    'category64' => [
        'title' => 'voluptas iusto',
        'depth' => 2,
        'description' => 'Libero et veniam molestiae dolore esse eos.',
    ],
    'category65' => [
        'title' => 'eaque deserunt',
        'depth' => 3,
        'description' => 'Commodi sed reprehenderit omnis et eaque.',
    ],
    'category66' => [
        'title' => 'cupiditate animi',
        'depth' => 2,
        'description' => 'Rem occaecati corporis et quo quia occaecati.',
    ],
    'category67' => [
        'title' => 'vel maiores',
        'depth' => 3,
        'description' => 'Nulla tempore deleniti saepe et sunt dolorum.',
    ],
    'category68' => [
        'title' => 'non adipisci',
        'depth' => 3,
        'description' => 'Totam non qui aut autem est et.',
    ],
    'category69' => [
        'title' => 'corrupti doloremque',
        'depth' => 2,
        'description' => 'Nemo ut ut et ut magnam dolor.',
    ],
    'category70' => [
        'title' => 'ad est',
        'depth' => 2,
        'description' => 'Quae odio dolores omnis expedita.',
    ],
    'category71' => [
        'title' => 'est totam',
        'depth' => 3,
        'description' => 'Itaque perspiciatis accusamus aut debitis et.',
    ],
    'category72' => [
        'title' => 'ut praesentium',
        'depth' => 2,
        'description' => 'Voluptas numquam autem accusamus at iste.',
    ],
    'category73' => [
        'title' => 'sunt voluptatem',
        'depth' => 3,
        'description' => 'Incidunt ut nam dicta repudiandae aliquam.',
    ],
    'category74' => [
        'title' => 'voluptate odit',
        'depth' => 1,
        'description' => 'Sunt nihil cupiditate ad quia asperiores.',
    ],
    'category75' => [
        'title' => 'autem est',
        'depth' => 2,
        'description' => 'Reiciendis aut fuga modi facere.',
    ],
    'category76' => [
        'title' => 'accusamus quibusdam',
        'depth' => 1,
        'description' => 'Omnis et ullam quaerat qui perspiciatis aliquam.',
    ],
    'category77' => [
        'title' => 'culpa nemo',
        'depth' => 2,
        'description' => 'Ab voluptate dolorem illum repellat voluptatum.',
    ],
    'category78' => [
        'title' => 'veritatis debitis',
        'depth' => 2,
        'description' => 'Dolorem consequuntur rerum minima et itaque.',
    ],
    'category79' => [
        'title' => 'corporis est',
        'depth' => 1,
        'description' => 'Accusamus odit molestiae modi labore quia.',
    ],
    'category80' => [
        'title' => 'ea quasi',
        'depth' => 2,
        'description' => 'Impedit atque libero voluptatem saepe velit quo.',
    ],
    'category81' => [
        'title' => 'rerum in',
        'depth' => 3,
        'description' => 'Velit facere cupiditate voluptate odit et ut.',
    ],
    'category82' => [
        'title' => 'ad sapiente',
        'depth' => 2,
        'description' => 'Assumenda totam similique itaque voluptatem.',
    ],
    'category83' => [
        'title' => 'corporis officiis',
        'depth' => 3,
        'description' => 'Et est harum cum esse et quis.',
    ],
    'category84' => [
        'title' => 'sit non',
        'depth' => 1,
        'description' => 'Aperiam eos blanditiis ut voluptatum et.',
    ],
    'category85' => [
        'title' => 'voluptas temporibus',
        'depth' => 2,
        'description' => 'Quis ad eveniet voluptatem neque.',
    ],
    'category86' => [
        'title' => 'repellendus ipsam',
        'depth' => 2,
        'description' => 'Reprehenderit sint et blanditiis nihil quia.',
    ],
    'category87' => [
        'title' => 'ut qui',
        'depth' => 1,
        'description' => 'Sint magni aut laudantium suscipit est itaque.',
    ],
    'category88' => [
        'title' => 'est et',
        'depth' => 2,
        'description' => 'Voluptas quia voluptatum voluptatem.',
    ],
    'category89' => [
        'title' => 'minus omnis',
        'depth' => 2,
        'description' => 'Accusamus doloremque perferendis minus mollitia.',
    ],
    'category90' => [
        'title' => 'et debitis',
        'depth' => 1,
        'description' => 'Est alias nihil est.',
    ],
    'category91' => [
        'title' => 'et mollitia',
        'depth' => 2,
        'description' => 'Et voluptatum et cumque.',
    ],
    'category92' => [
        'title' => 'ea ipsum',
        'depth' => 2,
        'description' => 'Numquam odio iure alias.',
    ],
    'category93' => [
        'title' => 'laudantium alias',
        'depth' => 3,
        'description' => 'Quasi est ducimus minus veniam dolor autem.',
    ],
    'category94' => [
        'title' => 'accusamus corporis',
        'depth' => 2,
        'description' => 'Aliquid tempora dolore amet corporis aut.',
    ],
    'category95' => [
        'title' => 'aut et',
        'depth' => 3,
        'description' => 'Reiciendis non in sint ea.',
    ],
    'category96' => [
        'title' => 'suscipit et',
        'depth' => 1,
        'description' => 'Eligendi accusamus est architecto ad commodi.',
    ],
    'category97' => [
        'title' => 'odio et',
        'depth' => 1,
        'description' => 'Dolor ratione numquam perferendis odit molestias.',
    ],
    'category98' => [
        'title' => 'similique iusto',
        'depth' => 3,
        'description' => 'Consequatur neque voluptatem et.',
    ],
    'category99' => [
        'title' => 'omnis dolorum',
        'depth' => 1,
        'description' => 'Et nostrum iste deleniti vel aut.',
    ],
];
