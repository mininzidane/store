<?php

return [
    'product_list_to_product0' => [
        'product_list_id' => 21,
        'product_id' => 15,
        'quantity' => 7,
    ],
    'product_list_to_product1' => [
        'product_list_id' => 10,
        'product_id' => 94,
        'quantity' => 8,
    ],
    'product_list_to_product2' => [
        'product_list_id' => 29,
        'product_id' => 8,
        'quantity' => 9,
    ],
    'product_list_to_product3' => [
        'product_list_id' => 30,
        'product_id' => 20,
        'quantity' => 5,
    ],
    'product_list_to_product4' => [
        'product_list_id' => 5,
        'product_id' => 69,
        'quantity' => 5,
    ],
    'product_list_to_product5' => [
        'product_list_id' => 21,
        'product_id' => 86,
        'quantity' => 3,
    ],
    'product_list_to_product6' => [
        'product_list_id' => 17,
        'product_id' => 25,
        'quantity' => 10,
    ],
    'product_list_to_product7' => [
        'product_list_id' => 26,
        'product_id' => 37,
        'quantity' => 6,
    ],
    'product_list_to_product8' => [
        'product_list_id' => 14,
        'product_id' => 94,
        'quantity' => 7,
    ],
    'product_list_to_product9' => [
        'product_list_id' => 3,
        'product_id' => 48,
        'quantity' => 4,
    ],
    'product_list_to_product10' => [
        'product_list_id' => 8,
        'product_id' => 58,
        'quantity' => 5,
    ],
    'product_list_to_product11' => [
        'product_list_id' => 25,
        'product_id' => 57,
        'quantity' => 10,
    ],
    'product_list_to_product12' => [
        'product_list_id' => 5,
        'product_id' => 33,
        'quantity' => 9,
    ],
    'product_list_to_product13' => [
        'product_list_id' => 13,
        'product_id' => 38,
        'quantity' => 5,
    ],
    'product_list_to_product14' => [
        'product_list_id' => 30,
        'product_id' => 27,
        'quantity' => 10,
    ],
    'product_list_to_product15' => [
        'product_list_id' => 13,
        'product_id' => 38,
        'quantity' => 10,
    ],
    'product_list_to_product16' => [
        'product_list_id' => 26,
        'product_id' => 25,
        'quantity' => 9,
    ],
    'product_list_to_product17' => [
        'product_list_id' => 19,
        'product_id' => 44,
        'quantity' => 2,
    ],
    'product_list_to_product18' => [
        'product_list_id' => 30,
        'product_id' => 93,
        'quantity' => 5,
    ],
    'product_list_to_product19' => [
        'product_list_id' => 8,
        'product_id' => 31,
        'quantity' => 4,
    ],
    'product_list_to_product20' => [
        'product_list_id' => 23,
        'product_id' => 63,
        'quantity' => 3,
    ],
    'product_list_to_product21' => [
        'product_list_id' => 8,
        'product_id' => 50,
        'quantity' => 4,
    ],
    'product_list_to_product22' => [
        'product_list_id' => 1,
        'product_id' => 6,
        'quantity' => 3,
    ],
    'product_list_to_product23' => [
        'product_list_id' => 2,
        'product_id' => 11,
        'quantity' => 4,
    ],
    'product_list_to_product24' => [
        'product_list_id' => 6,
        'product_id' => 94,
        'quantity' => 5,
    ],
    'product_list_to_product25' => [
        'product_list_id' => 13,
        'product_id' => 22,
        'quantity' => 8,
    ],
    'product_list_to_product26' => [
        'product_list_id' => 5,
        'product_id' => 73,
        'quantity' => 10,
    ],
    'product_list_to_product27' => [
        'product_list_id' => 6,
        'product_id' => 58,
        'quantity' => 10,
    ],
    'product_list_to_product28' => [
        'product_list_id' => 1,
        'product_id' => 87,
        'quantity' => 4,
    ],
    'product_list_to_product29' => [
        'product_list_id' => 8,
        'product_id' => 85,
        'quantity' => 2,
    ],
    'product_list_to_product30' => [
        'product_list_id' => 25,
        'product_id' => 38,
        'quantity' => 7,
    ],
    'product_list_to_product31' => [
        'product_list_id' => 27,
        'product_id' => 68,
        'quantity' => 7,
    ],
    'product_list_to_product32' => [
        'product_list_id' => 13,
        'product_id' => 40,
        'quantity' => 9,
    ],
    'product_list_to_product33' => [
        'product_list_id' => 12,
        'product_id' => 34,
        'quantity' => 2,
    ],
    'product_list_to_product34' => [
        'product_list_id' => 14,
        'product_id' => 68,
        'quantity' => 2,
    ],
    'product_list_to_product35' => [
        'product_list_id' => 7,
        'product_id' => 5,
        'quantity' => 9,
    ],
    'product_list_to_product36' => [
        'product_list_id' => 6,
        'product_id' => 97,
        'quantity' => 7,
    ],
    'product_list_to_product37' => [
        'product_list_id' => 15,
        'product_id' => 4,
        'quantity' => 4,
    ],
    'product_list_to_product38' => [
        'product_list_id' => 12,
        'product_id' => 37,
        'quantity' => 5,
    ],
    'product_list_to_product39' => [
        'product_list_id' => 26,
        'product_id' => 53,
        'quantity' => 3,
    ],
    'product_list_to_product40' => [
        'product_list_id' => 13,
        'product_id' => 49,
        'quantity' => 2,
    ],
    'product_list_to_product41' => [
        'product_list_id' => 8,
        'product_id' => 58,
        'quantity' => 6,
    ],
    'product_list_to_product42' => [
        'product_list_id' => 27,
        'product_id' => 28,
        'quantity' => 7,
    ],
    'product_list_to_product43' => [
        'product_list_id' => 27,
        'product_id' => 86,
        'quantity' => 6,
    ],
    'product_list_to_product44' => [
        'product_list_id' => 17,
        'product_id' => 85,
        'quantity' => 8,
    ],
    'product_list_to_product45' => [
        'product_list_id' => 8,
        'product_id' => 88,
        'quantity' => 2,
    ],
    'product_list_to_product46' => [
        'product_list_id' => 1,
        'product_id' => 15,
        'quantity' => 9,
    ],
    'product_list_to_product47' => [
        'product_list_id' => 23,
        'product_id' => 96,
        'quantity' => 3,
    ],
    'product_list_to_product48' => [
        'product_list_id' => 4,
        'product_id' => 33,
        'quantity' => 3,
    ],
    'product_list_to_product49' => [
        'product_list_id' => 14,
        'product_id' => 39,
        'quantity' => 4,
    ],
    'product_list_to_product50' => [
        'product_list_id' => 10,
        'product_id' => 84,
        'quantity' => 8,
    ],
    'product_list_to_product51' => [
        'product_list_id' => 29,
        'product_id' => 76,
        'quantity' => 7,
    ],
    'product_list_to_product52' => [
        'product_list_id' => 24,
        'product_id' => 68,
        'quantity' => 5,
    ],
    'product_list_to_product53' => [
        'product_list_id' => 23,
        'product_id' => 85,
        'quantity' => 1,
    ],
    'product_list_to_product54' => [
        'product_list_id' => 2,
        'product_id' => 64,
        'quantity' => 7,
    ],
    'product_list_to_product55' => [
        'product_list_id' => 1,
        'product_id' => 14,
        'quantity' => 1,
    ],
    'product_list_to_product56' => [
        'product_list_id' => 14,
        'product_id' => 32,
        'quantity' => 2,
    ],
    'product_list_to_product57' => [
        'product_list_id' => 1,
        'product_id' => 13,
        'quantity' => 5,
    ],
    'product_list_to_product58' => [
        'product_list_id' => 20,
        'product_id' => 48,
        'quantity' => 8,
    ],
    'product_list_to_product59' => [
        'product_list_id' => 8,
        'product_id' => 83,
        'quantity' => 3,
    ],
    'product_list_to_product60' => [
        'product_list_id' => 16,
        'product_id' => 72,
        'quantity' => 3,
    ],
    'product_list_to_product61' => [
        'product_list_id' => 9,
        'product_id' => 58,
        'quantity' => 5,
    ],
    'product_list_to_product62' => [
        'product_list_id' => 23,
        'product_id' => 50,
        'quantity' => 1,
    ],
    'product_list_to_product63' => [
        'product_list_id' => 9,
        'product_id' => 18,
        'quantity' => 9,
    ],
    'product_list_to_product64' => [
        'product_list_id' => 22,
        'product_id' => 64,
        'quantity' => 1,
    ],
    'product_list_to_product65' => [
        'product_list_id' => 24,
        'product_id' => 24,
        'quantity' => 8,
    ],
    'product_list_to_product66' => [
        'product_list_id' => 17,
        'product_id' => 10,
        'quantity' => 8,
    ],
    'product_list_to_product67' => [
        'product_list_id' => 2,
        'product_id' => 59,
        'quantity' => 7,
    ],
    'product_list_to_product68' => [
        'product_list_id' => 19,
        'product_id' => 16,
        'quantity' => 1,
    ],
    'product_list_to_product69' => [
        'product_list_id' => 22,
        'product_id' => 39,
        'quantity' => 8,
    ],
    'product_list_to_product70' => [
        'product_list_id' => 4,
        'product_id' => 7,
        'quantity' => 8,
    ],
    'product_list_to_product71' => [
        'product_list_id' => 26,
        'product_id' => 15,
        'quantity' => 10,
    ],
    'product_list_to_product72' => [
        'product_list_id' => 27,
        'product_id' => 93,
        'quantity' => 3,
    ],
    'product_list_to_product73' => [
        'product_list_id' => 25,
        'product_id' => 81,
        'quantity' => 4,
    ],
    'product_list_to_product74' => [
        'product_list_id' => 25,
        'product_id' => 37,
        'quantity' => 8,
    ],
    'product_list_to_product75' => [
        'product_list_id' => 29,
        'product_id' => 76,
        'quantity' => 2,
    ],
    'product_list_to_product76' => [
        'product_list_id' => 20,
        'product_id' => 21,
        'quantity' => 9,
    ],
    'product_list_to_product77' => [
        'product_list_id' => 26,
        'product_id' => 88,
        'quantity' => 5,
    ],
    'product_list_to_product78' => [
        'product_list_id' => 14,
        'product_id' => 37,
        'quantity' => 3,
    ],
    'product_list_to_product79' => [
        'product_list_id' => 28,
        'product_id' => 95,
        'quantity' => 3,
    ],
    'product_list_to_product80' => [
        'product_list_id' => 27,
        'product_id' => 10,
        'quantity' => 3,
    ],
    'product_list_to_product81' => [
        'product_list_id' => 6,
        'product_id' => 32,
        'quantity' => 7,
    ],
    'product_list_to_product82' => [
        'product_list_id' => 22,
        'product_id' => 10,
        'quantity' => 2,
    ],
    'product_list_to_product83' => [
        'product_list_id' => 25,
        'product_id' => 63,
        'quantity' => 7,
    ],
    'product_list_to_product84' => [
        'product_list_id' => 7,
        'product_id' => 36,
        'quantity' => 1,
    ],
    'product_list_to_product85' => [
        'product_list_id' => 30,
        'product_id' => 5,
        'quantity' => 7,
    ],
    'product_list_to_product86' => [
        'product_list_id' => 8,
        'product_id' => 65,
        'quantity' => 6,
    ],
    'product_list_to_product87' => [
        'product_list_id' => 24,
        'product_id' => 28,
        'quantity' => 10,
    ],
    'product_list_to_product88' => [
        'product_list_id' => 18,
        'product_id' => 13,
        'quantity' => 7,
    ],
    'product_list_to_product89' => [
        'product_list_id' => 27,
        'product_id' => 44,
        'quantity' => 6,
    ],
    'product_list_to_product90' => [
        'product_list_id' => 21,
        'product_id' => 82,
        'quantity' => 2,
    ],
    'product_list_to_product91' => [
        'product_list_id' => 18,
        'product_id' => 15,
        'quantity' => 2,
    ],
    'product_list_to_product92' => [
        'product_list_id' => 22,
        'product_id' => 100,
        'quantity' => 3,
    ],
    'product_list_to_product93' => [
        'product_list_id' => 25,
        'product_id' => 64,
        'quantity' => 8,
    ],
    'product_list_to_product94' => [
        'product_list_id' => 1,
        'product_id' => 24,
        'quantity' => 8,
    ],
    'product_list_to_product95' => [
        'product_list_id' => 19,
        'product_id' => 71,
        'quantity' => 9,
    ],
    'product_list_to_product96' => [
        'product_list_id' => 16,
        'product_id' => 99,
        'quantity' => 3,
    ],
    'product_list_to_product97' => [
        'product_list_id' => 12,
        'product_id' => 95,
        'quantity' => 3,
    ],
    'product_list_to_product98' => [
        'product_list_id' => 28,
        'product_id' => 85,
        'quantity' => 8,
    ],
    'product_list_to_product99' => [
        'product_list_id' => 29,
        'product_id' => 35,
        'quantity' => 4,
    ],
];
