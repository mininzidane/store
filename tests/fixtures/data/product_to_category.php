<?php

return [
    'product_to_category0' => [
        'product_id' => 89,
        'category_id' => 35,
    ],
    'product_to_category1' => [
        'product_id' => 70,
        'category_id' => 55,
    ],
    'product_to_category2' => [
        'product_id' => 74,
        'category_id' => 4,
    ],
    'product_to_category3' => [
        'product_id' => 65,
        'category_id' => 58,
    ],
    'product_to_category4' => [
        'product_id' => 8,
        'category_id' => 31,
    ],
    'product_to_category5' => [
        'product_id' => 57,
        'category_id' => 48,
    ],
    'product_to_category6' => [
        'product_id' => 89,
        'category_id' => 72,
    ],
    'product_to_category7' => [
        'product_id' => 66,
        'category_id' => 52,
    ],
    'product_to_category8' => [
        'product_id' => 81,
        'category_id' => 86,
    ],
    'product_to_category9' => [
        'product_id' => 83,
        'category_id' => 6,
    ],
    'product_to_category10' => [
        'product_id' => 47,
        'category_id' => 62,
    ],
    'product_to_category11' => [
        'product_id' => 14,
        'category_id' => 3,
    ],
    'product_to_category12' => [
        'product_id' => 94,
        'category_id' => 56,
    ],
    'product_to_category13' => [
        'product_id' => 37,
        'category_id' => 90,
    ],
    'product_to_category14' => [
        'product_id' => 12,
        'category_id' => 67,
    ],
    'product_to_category15' => [
        'product_id' => 28,
        'category_id' => 23,
    ],
    'product_to_category16' => [
        'product_id' => 58,
        'category_id' => 97,
    ],
    'product_to_category17' => [
        'product_id' => 33,
        'category_id' => 99,
    ],
    'product_to_category18' => [
        'product_id' => 55,
        'category_id' => 60,
    ],
    'product_to_category19' => [
        'product_id' => 13,
        'category_id' => 97,
    ],
    'product_to_category20' => [
        'product_id' => 86,
        'category_id' => 76,
    ],
    'product_to_category21' => [
        'product_id' => 11,
        'category_id' => 6,
    ],
    'product_to_category22' => [
        'product_id' => 41,
        'category_id' => 39,
    ],
    'product_to_category23' => [
        'product_id' => 26,
        'category_id' => 52,
    ],
    'product_to_category24' => [
        'product_id' => 71,
        'category_id' => 38,
    ],
    'product_to_category25' => [
        'product_id' => 84,
        'category_id' => 2,
    ],
    'product_to_category26' => [
        'product_id' => 87,
        'category_id' => 34,
    ],
    'product_to_category27' => [
        'product_id' => 80,
        'category_id' => 97,
    ],
    'product_to_category28' => [
        'product_id' => 7,
        'category_id' => 18,
    ],
    'product_to_category29' => [
        'product_id' => 58,
        'category_id' => 56,
    ],
    'product_to_category30' => [
        'product_id' => 66,
        'category_id' => 40,
    ],
    'product_to_category31' => [
        'product_id' => 23,
        'category_id' => 7,
    ],
    'product_to_category32' => [
        'product_id' => 33,
        'category_id' => 56,
    ],
    'product_to_category33' => [
        'product_id' => 66,
        'category_id' => 36,
    ],
    'product_to_category34' => [
        'product_id' => 35,
        'category_id' => 75,
    ],
    'product_to_category35' => [
        'product_id' => 51,
        'category_id' => 58,
    ],
    'product_to_category36' => [
        'product_id' => 79,
        'category_id' => 64,
    ],
    'product_to_category37' => [
        'product_id' => 88,
        'category_id' => 17,
    ],
    'product_to_category38' => [
        'product_id' => 7,
        'category_id' => 52,
    ],
    'product_to_category39' => [
        'product_id' => 18,
        'category_id' => 98,
    ],
    'product_to_category40' => [
        'product_id' => 40,
        'category_id' => 17,
    ],
    'product_to_category41' => [
        'product_id' => 50,
        'category_id' => 87,
    ],
    'product_to_category42' => [
        'product_id' => 21,
        'category_id' => 55,
    ],
    'product_to_category43' => [
        'product_id' => 98,
        'category_id' => 76,
    ],
    'product_to_category44' => [
        'product_id' => 36,
        'category_id' => 71,
    ],
    'product_to_category45' => [
        'product_id' => 9,
        'category_id' => 48,
    ],
    'product_to_category46' => [
        'product_id' => 45,
        'category_id' => 54,
    ],
    'product_to_category47' => [
        'product_id' => 25,
        'category_id' => 50,
    ],
    'product_to_category48' => [
        'product_id' => 75,
        'category_id' => 90,
    ],
    'product_to_category49' => [
        'product_id' => 83,
        'category_id' => 61,
    ],
    'product_to_category50' => [
        'product_id' => 89,
        'category_id' => 10,
    ],
    'product_to_category51' => [
        'product_id' => 79,
        'category_id' => 3,
    ],
    'product_to_category52' => [
        'product_id' => 61,
        'category_id' => 43,
    ],
    'product_to_category53' => [
        'product_id' => 63,
        'category_id' => 23,
    ],
    'product_to_category54' => [
        'product_id' => 47,
        'category_id' => 88,
    ],
    'product_to_category55' => [
        'product_id' => 73,
        'category_id' => 26,
    ],
    'product_to_category56' => [
        'product_id' => 9,
        'category_id' => 79,
    ],
    'product_to_category57' => [
        'product_id' => 41,
        'category_id' => 61,
    ],
    'product_to_category58' => [
        'product_id' => 37,
        'category_id' => 40,
    ],
    'product_to_category59' => [
        'product_id' => 64,
        'category_id' => 6,
    ],
    'product_to_category60' => [
        'product_id' => 92,
        'category_id' => 99,
    ],
    'product_to_category61' => [
        'product_id' => 66,
        'category_id' => 50,
    ],
    'product_to_category62' => [
        'product_id' => 85,
        'category_id' => 98,
    ],
    'product_to_category63' => [
        'product_id' => 11,
        'category_id' => 54,
    ],
    'product_to_category64' => [
        'product_id' => 78,
        'category_id' => 47,
    ],
    'product_to_category65' => [
        'product_id' => 100,
        'category_id' => 24,
    ],
    'product_to_category66' => [
        'product_id' => 87,
        'category_id' => 54,
    ],
    'product_to_category67' => [
        'product_id' => 99,
        'category_id' => 36,
    ],
    'product_to_category68' => [
        'product_id' => 69,
        'category_id' => 96,
    ],
    'product_to_category69' => [
        'product_id' => 23,
        'category_id' => 99,
    ],
    'product_to_category70' => [
        'product_id' => 74,
        'category_id' => 57,
    ],
    'product_to_category71' => [
        'product_id' => 39,
        'category_id' => 95,
    ],
    'product_to_category72' => [
        'product_id' => 42,
        'category_id' => 3,
    ],
    'product_to_category73' => [
        'product_id' => 63,
        'category_id' => 37,
    ],
    'product_to_category74' => [
        'product_id' => 100,
        'category_id' => 92,
    ],
    'product_to_category75' => [
        'product_id' => 21,
        'category_id' => 10,
    ],
    'product_to_category76' => [
        'product_id' => 51,
        'category_id' => 71,
    ],
    'product_to_category77' => [
        'product_id' => 97,
        'category_id' => 88,
    ],
    'product_to_category78' => [
        'product_id' => 15,
        'category_id' => 31,
    ],
    'product_to_category79' => [
        'product_id' => 76,
        'category_id' => 30,
    ],
    'product_to_category80' => [
        'product_id' => 71,
        'category_id' => 52,
    ],
    'product_to_category81' => [
        'product_id' => 17,
        'category_id' => 55,
    ],
    'product_to_category82' => [
        'product_id' => 57,
        'category_id' => 6,
    ],
    'product_to_category83' => [
        'product_id' => 40,
        'category_id' => 77,
    ],
    'product_to_category84' => [
        'product_id' => 81,
        'category_id' => 45,
    ],
    'product_to_category85' => [
        'product_id' => 31,
        'category_id' => 89,
    ],
    'product_to_category86' => [
        'product_id' => 48,
        'category_id' => 42,
    ],
    'product_to_category87' => [
        'product_id' => 44,
        'category_id' => 10,
    ],
    'product_to_category88' => [
        'product_id' => 8,
        'category_id' => 1,
    ],
    'product_to_category89' => [
        'product_id' => 78,
        'category_id' => 10,
    ],
    'product_to_category90' => [
        'product_id' => 58,
        'category_id' => 13,
    ],
    'product_to_category91' => [
        'product_id' => 11,
        'category_id' => 91,
    ],
    'product_to_category92' => [
        'product_id' => 91,
        'category_id' => 70,
    ],
    'product_to_category93' => [
        'product_id' => 47,
        'category_id' => 53,
    ],
    'product_to_category94' => [
        'product_id' => 75,
        'category_id' => 64,
    ],
    'product_to_category95' => [
        'product_id' => 76,
        'category_id' => 69,
    ],
    'product_to_category96' => [
        'product_id' => 27,
        'category_id' => 91,
    ],
    'product_to_category97' => [
        'product_id' => 2,
        'category_id' => 84,
    ],
    'product_to_category98' => [
        'product_id' => 32,
        'category_id' => 94,
    ],
    'product_to_category99' => [
        'product_id' => 79,
        'category_id' => 11,
    ],
    'product_to_category100' => [
        'product_id' => 76,
        'category_id' => 51,
    ],
    'product_to_category101' => [
        'product_id' => 29,
        'category_id' => 53,
    ],
    'product_to_category102' => [
        'product_id' => 53,
        'category_id' => 16,
    ],
    'product_to_category103' => [
        'product_id' => 41,
        'category_id' => 33,
    ],
    'product_to_category104' => [
        'product_id' => 85,
        'category_id' => 94,
    ],
    'product_to_category105' => [
        'product_id' => 45,
        'category_id' => 23,
    ],
    'product_to_category106' => [
        'product_id' => 26,
        'category_id' => 52,
    ],
    'product_to_category107' => [
        'product_id' => 9,
        'category_id' => 65,
    ],
    'product_to_category108' => [
        'product_id' => 43,
        'category_id' => 8,
    ],
    'product_to_category109' => [
        'product_id' => 80,
        'category_id' => 7,
    ],
    'product_to_category110' => [
        'product_id' => 26,
        'category_id' => 73,
    ],
    'product_to_category111' => [
        'product_id' => 29,
        'category_id' => 73,
    ],
    'product_to_category112' => [
        'product_id' => 16,
        'category_id' => 44,
    ],
    'product_to_category113' => [
        'product_id' => 66,
        'category_id' => 5,
    ],
    'product_to_category114' => [
        'product_id' => 44,
        'category_id' => 49,
    ],
    'product_to_category115' => [
        'product_id' => 14,
        'category_id' => 45,
    ],
    'product_to_category116' => [
        'product_id' => 2,
        'category_id' => 70,
    ],
    'product_to_category117' => [
        'product_id' => 61,
        'category_id' => 14,
    ],
    'product_to_category118' => [
        'product_id' => 39,
        'category_id' => 16,
    ],
    'product_to_category119' => [
        'product_id' => 100,
        'category_id' => 91,
    ],
    'product_to_category120' => [
        'product_id' => 65,
        'category_id' => 63,
    ],
    'product_to_category121' => [
        'product_id' => 58,
        'category_id' => 95,
    ],
    'product_to_category122' => [
        'product_id' => 73,
        'category_id' => 87,
    ],
    'product_to_category123' => [
        'product_id' => 5,
        'category_id' => 36,
    ],
    'product_to_category124' => [
        'product_id' => 84,
        'category_id' => 87,
    ],
    'product_to_category125' => [
        'product_id' => 81,
        'category_id' => 60,
    ],
    'product_to_category126' => [
        'product_id' => 85,
        'category_id' => 56,
    ],
    'product_to_category127' => [
        'product_id' => 73,
        'category_id' => 63,
    ],
    'product_to_category128' => [
        'product_id' => 86,
        'category_id' => 70,
    ],
    'product_to_category129' => [
        'product_id' => 65,
        'category_id' => 37,
    ],
    'product_to_category130' => [
        'product_id' => 45,
        'category_id' => 43,
    ],
    'product_to_category131' => [
        'product_id' => 33,
        'category_id' => 67,
    ],
    'product_to_category132' => [
        'product_id' => 24,
        'category_id' => 65,
    ],
    'product_to_category133' => [
        'product_id' => 22,
        'category_id' => 85,
    ],
    'product_to_category134' => [
        'product_id' => 60,
        'category_id' => 29,
    ],
    'product_to_category135' => [
        'product_id' => 74,
        'category_id' => 26,
    ],
    'product_to_category136' => [
        'product_id' => 27,
        'category_id' => 38,
    ],
    'product_to_category137' => [
        'product_id' => 22,
        'category_id' => 69,
    ],
    'product_to_category138' => [
        'product_id' => 22,
        'category_id' => 42,
    ],
    'product_to_category139' => [
        'product_id' => 17,
        'category_id' => 90,
    ],
    'product_to_category140' => [
        'product_id' => 54,
        'category_id' => 72,
    ],
    'product_to_category141' => [
        'product_id' => 85,
        'category_id' => 5,
    ],
    'product_to_category142' => [
        'product_id' => 70,
        'category_id' => 45,
    ],
    'product_to_category143' => [
        'product_id' => 70,
        'category_id' => 64,
    ],
    'product_to_category144' => [
        'product_id' => 90,
        'category_id' => 78,
    ],
    'product_to_category145' => [
        'product_id' => 8,
        'category_id' => 17,
    ],
    'product_to_category146' => [
        'product_id' => 86,
        'category_id' => 37,
    ],
    'product_to_category147' => [
        'product_id' => 31,
        'category_id' => 53,
    ],
    'product_to_category148' => [
        'product_id' => 66,
        'category_id' => 3,
    ],
    'product_to_category149' => [
        'product_id' => 53,
        'category_id' => 16,
    ],
    'product_to_category150' => [
        'product_id' => 18,
        'category_id' => 96,
    ],
    'product_to_category151' => [
        'product_id' => 70,
        'category_id' => 63,
    ],
    'product_to_category152' => [
        'product_id' => 51,
        'category_id' => 33,
    ],
    'product_to_category153' => [
        'product_id' => 18,
        'category_id' => 44,
    ],
    'product_to_category154' => [
        'product_id' => 27,
        'category_id' => 99,
    ],
    'product_to_category155' => [
        'product_id' => 35,
        'category_id' => 17,
    ],
    'product_to_category156' => [
        'product_id' => 40,
        'category_id' => 46,
    ],
    'product_to_category157' => [
        'product_id' => 7,
        'category_id' => 62,
    ],
    'product_to_category158' => [
        'product_id' => 35,
        'category_id' => 82,
    ],
    'product_to_category159' => [
        'product_id' => 58,
        'category_id' => 61,
    ],
    'product_to_category160' => [
        'product_id' => 37,
        'category_id' => 30,
    ],
    'product_to_category161' => [
        'product_id' => 56,
        'category_id' => 44,
    ],
    'product_to_category162' => [
        'product_id' => 89,
        'category_id' => 97,
    ],
    'product_to_category163' => [
        'product_id' => 40,
        'category_id' => 25,
    ],
    'product_to_category164' => [
        'product_id' => 87,
        'category_id' => 55,
    ],
    'product_to_category165' => [
        'product_id' => 94,
        'category_id' => 78,
    ],
    'product_to_category166' => [
        'product_id' => 73,
        'category_id' => 75,
    ],
    'product_to_category167' => [
        'product_id' => 7,
        'category_id' => 27,
    ],
    'product_to_category168' => [
        'product_id' => 9,
        'category_id' => 10,
    ],
    'product_to_category169' => [
        'product_id' => 11,
        'category_id' => 62,
    ],
    'product_to_category170' => [
        'product_id' => 44,
        'category_id' => 84,
    ],
    'product_to_category171' => [
        'product_id' => 43,
        'category_id' => 87,
    ],
    'product_to_category172' => [
        'product_id' => 31,
        'category_id' => 73,
    ],
    'product_to_category173' => [
        'product_id' => 68,
        'category_id' => 22,
    ],
    'product_to_category174' => [
        'product_id' => 12,
        'category_id' => 62,
    ],
    'product_to_category175' => [
        'product_id' => 83,
        'category_id' => 67,
    ],
    'product_to_category176' => [
        'product_id' => 77,
        'category_id' => 39,
    ],
    'product_to_category177' => [
        'product_id' => 72,
        'category_id' => 55,
    ],
    'product_to_category178' => [
        'product_id' => 65,
        'category_id' => 66,
    ],
    'product_to_category179' => [
        'product_id' => 30,
        'category_id' => 17,
    ],
    'product_to_category180' => [
        'product_id' => 90,
        'category_id' => 53,
    ],
    'product_to_category181' => [
        'product_id' => 27,
        'category_id' => 14,
    ],
    'product_to_category182' => [
        'product_id' => 19,
        'category_id' => 44,
    ],
    'product_to_category183' => [
        'product_id' => 45,
        'category_id' => 62,
    ],
    'product_to_category184' => [
        'product_id' => 24,
        'category_id' => 59,
    ],
    'product_to_category185' => [
        'product_id' => 59,
        'category_id' => 63,
    ],
    'product_to_category186' => [
        'product_id' => 99,
        'category_id' => 53,
    ],
    'product_to_category187' => [
        'product_id' => 81,
        'category_id' => 2,
    ],
    'product_to_category188' => [
        'product_id' => 59,
        'category_id' => 56,
    ],
    'product_to_category189' => [
        'product_id' => 67,
        'category_id' => 74,
    ],
    'product_to_category190' => [
        'product_id' => 91,
        'category_id' => 22,
    ],
    'product_to_category191' => [
        'product_id' => 11,
        'category_id' => 59,
    ],
    'product_to_category192' => [
        'product_id' => 70,
        'category_id' => 52,
    ],
    'product_to_category193' => [
        'product_id' => 62,
        'category_id' => 77,
    ],
    'product_to_category194' => [
        'product_id' => 81,
        'category_id' => 75,
    ],
    'product_to_category195' => [
        'product_id' => 60,
        'category_id' => 79,
    ],
    'product_to_category196' => [
        'product_id' => 46,
        'category_id' => 83,
    ],
    'product_to_category197' => [
        'product_id' => 13,
        'category_id' => 65,
    ],
    'product_to_category198' => [
        'product_id' => 38,
        'category_id' => 68,
    ],
    'product_to_category199' => [
        'product_id' => 58,
        'category_id' => 58,
    ],
];
