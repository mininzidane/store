<?php

return [
    'product0' => [
        'title' => 'nam adipisci',
        'description' => 'Nesciunt explicabo aut et.',
    ],
    'product1' => [
        'title' => 'totam molestias',
        'description' => 'Autem eaque dolore possimus.',
    ],
    'product2' => [
        'title' => 'velit eum',
        'description' => 'Et eius molestias deserunt aliquid.',
    ],
    'product3' => [
        'title' => 'nulla alias',
        'description' => 'Omnis culpa ut beatae commodi repudiandae.',
    ],
    'product4' => [
        'title' => 'accusamus voluptatum',
        'description' => 'Debitis sint nobis veritatis et minima.',
    ],
    'product5' => [
        'title' => 'aut nobis',
        'description' => 'Quisquam occaecati earum perferendis quaerat.',
    ],
    'product6' => [
        'title' => 'quo quia',
        'description' => 'Maiores qui voluptas pariatur repudiandae ad.',
    ],
    'product7' => [
        'title' => 'illo ut',
        'description' => 'Culpa quis laudantium inventore ea.',
    ],
    'product8' => [
        'title' => 'ea et',
        'description' => 'Nemo magnam ut qui atque.',
    ],
    'product9' => [
        'title' => 'aliquam ex',
        'description' => 'Aliquid in cumque enim.',
    ],
    'product10' => [
        'title' => 'autem dolorem',
        'description' => 'Cupiditate fugiat voluptatem molestias non.',
    ],
    'product11' => [
        'title' => 'neque a',
        'description' => 'Qui tenetur nam beatae aspernatur omnis aut aut.',
    ],
    'product12' => [
        'title' => 'eos quas',
        'description' => 'Laboriosam blanditiis ullam facere ea dolorum.',
    ],
    'product13' => [
        'title' => 'occaecati aut',
        'description' => 'Nesciunt est dolorem corporis dolor minus.',
    ],
    'product14' => [
        'title' => 'delectus voluptas',
        'description' => 'Vel voluptas aliquid adipisci.',
    ],
    'product15' => [
        'title' => 'dolor sunt',
        'description' => 'Labore expedita neque voluptatem deleniti.',
    ],
    'product16' => [
        'title' => 'assumenda dignissimos',
        'description' => 'Fugit ex autem enim dolores dolores repellat.',
    ],
    'product17' => [
        'title' => 'dolore sed',
        'description' => 'Inventore autem voluptatum non deserunt totam ut.',
    ],
    'product18' => [
        'title' => 'occaecati ea',
        'description' => 'Aut et voluptates dolor esse harum quidem eum.',
    ],
    'product19' => [
        'title' => 'provident et',
        'description' => 'Enim rerum est omnis laborum ea earum.',
    ],
    'product20' => [
        'title' => 'asperiores aut',
        'description' => 'Libero eius aliquam assumenda quia.',
    ],
    'product21' => [
        'title' => 'doloremque expedita',
        'description' => 'Et repellat sapiente veniam distinctio et et est.',
    ],
    'product22' => [
        'title' => 'ut ex',
        'description' => 'Minima vitae recusandae qui alias quidem.',
    ],
    'product23' => [
        'title' => 'expedita ad',
        'description' => 'Inventore omnis expedita ab quia aut aliquid.',
    ],
    'product24' => [
        'title' => 'ut ut',
        'description' => 'Ex porro qui dolorum accusamus officia.',
    ],
    'product25' => [
        'title' => 'ad possimus',
        'description' => 'Vitae rerum sint tempora facere incidunt.',
    ],
    'product26' => [
        'title' => 'est mollitia',
        'description' => 'Veniam tempora illum vero.',
    ],
    'product27' => [
        'title' => 'occaecati aut',
        'description' => 'Ea voluptatem consectetur ut.',
    ],
    'product28' => [
        'title' => 'ipsa ea',
        'description' => 'Facere quo et facilis nostrum iste corrupti.',
    ],
    'product29' => [
        'title' => 'quia eum',
        'description' => 'Ipsam quia id ut fugiat qui suscipit quas.',
    ],
    'product30' => [
        'title' => 'dolorem accusamus',
        'description' => 'Eum perferendis fugit unde ratione fugiat error.',
    ],
    'product31' => [
        'title' => 'praesentium aut',
        'description' => 'Aut laboriosam sequi incidunt animi velit.',
    ],
    'product32' => [
        'title' => 'expedita et',
        'description' => 'Fugit porro molestiae corporis id.',
    ],
    'product33' => [
        'title' => 'vero molestiae',
        'description' => 'Tempora eligendi autem perspiciatis delectus.',
    ],
    'product34' => [
        'title' => 'cupiditate possimus',
        'description' => 'Commodi ipsam consectetur velit ab.',
    ],
    'product35' => [
        'title' => 'numquam officia',
        'description' => 'Odit voluptas explicabo et perspiciatis quia rem.',
    ],
    'product36' => [
        'title' => 'ex dolorem',
        'description' => 'Nostrum aspernatur illum voluptas qui veritatis.',
    ],
    'product37' => [
        'title' => 'minus qui',
        'description' => 'Voluptatem vitae tenetur deserunt expedita in.',
    ],
    'product38' => [
        'title' => 'ipsa qui',
        'description' => 'Optio id perspiciatis aliquam eos iusto dolor.',
    ],
    'product39' => [
        'title' => 'odio harum',
        'description' => 'Consequatur quidem quia unde est voluptas dolore.',
    ],
    'product40' => [
        'title' => 'asperiores nihil',
        'description' => 'Rerum natus corporis est dolorum modi.',
    ],
    'product41' => [
        'title' => 'voluptas hic',
        'description' => 'Delectus dolores atque commodi.',
    ],
    'product42' => [
        'title' => 'unde consequatur',
        'description' => 'Excepturi dolorem itaque at deserunt suscipit ab.',
    ],
    'product43' => [
        'title' => 'et expedita',
        'description' => 'Vitae praesentium deserunt doloremque ex facilis.',
    ],
    'product44' => [
        'title' => 'quo eveniet',
        'description' => 'Fuga consequatur nihil mollitia nisi eum et.',
    ],
    'product45' => [
        'title' => 'facere et',
        'description' => 'Commodi distinctio et voluptas eum.',
    ],
    'product46' => [
        'title' => 'hic ducimus',
        'description' => 'Incidunt quia velit cupiditate et vel.',
    ],
    'product47' => [
        'title' => 'quidem corporis',
        'description' => 'Molestias non voluptas veritatis est.',
    ],
    'product48' => [
        'title' => 'voluptatem qui',
        'description' => 'Vel molestiae id qui consequatur facere voluptas.',
    ],
    'product49' => [
        'title' => 'omnis ea',
        'description' => 'Consequatur harum eveniet quia sed velit illum.',
    ],
    'product50' => [
        'title' => 'et rerum',
        'description' => 'Reprehenderit inventore dolore repudiandae.',
    ],
    'product51' => [
        'title' => 'quis illo',
        'description' => 'Vel aliquam et aut earum sed qui.',
    ],
    'product52' => [
        'title' => 'et aut',
        'description' => 'Dolorem iure et commodi facilis.',
    ],
    'product53' => [
        'title' => 'atque nihil',
        'description' => 'Voluptatem atque consequatur ratione ut.',
    ],
    'product54' => [
        'title' => 'ut autem',
        'description' => 'Repudiandae qui sed et sint totam aspernatur.',
    ],
    'product55' => [
        'title' => 'eum tempore',
        'description' => 'Et dolores nihil voluptas culpa est.',
    ],
    'product56' => [
        'title' => 'officiis atque',
        'description' => 'Velit perspiciatis et temporibus ut aut eos.',
    ],
    'product57' => [
        'title' => 'corrupti autem',
        'description' => 'Sint magni veniam quidem atque.',
    ],
    'product58' => [
        'title' => 'assumenda fugiat',
        'description' => 'Ipsam libero nulla minima officiis enim.',
    ],
    'product59' => [
        'title' => 'voluptatem sint',
        'description' => 'Reiciendis nobis sint ut.',
    ],
    'product60' => [
        'title' => 'voluptates at',
        'description' => 'In magni dolores non qui molestiae quo.',
    ],
    'product61' => [
        'title' => 'eos provident',
        'description' => 'Dolorum voluptate iure sequi est rerum.',
    ],
    'product62' => [
        'title' => 'occaecati at',
        'description' => 'Facere voluptas omnis veniam debitis ipsa nihil.',
    ],
    'product63' => [
        'title' => 'placeat perspiciatis',
        'description' => 'Molestiae asperiores totam numquam quis ut.',
    ],
    'product64' => [
        'title' => 'aut vero',
        'description' => 'Deleniti est molestiae sapiente.',
    ],
    'product65' => [
        'title' => 'eligendi mollitia',
        'description' => 'Tempore alias reiciendis aliquam aliquam quo non.',
    ],
    'product66' => [
        'title' => 'recusandae voluptatem',
        'description' => 'Aut iure qui est voluptatem provident aut in non.',
    ],
    'product67' => [
        'title' => 'voluptatibus enim',
        'description' => 'Dolore aut explicabo accusamus neque dignissimos.',
    ],
    'product68' => [
        'title' => 'deserunt suscipit',
        'description' => 'Sed nulla molestiae iusto ut magni provident.',
    ],
    'product69' => [
        'title' => 'commodi debitis',
        'description' => 'Perspiciatis aliquam neque officia maxime ab.',
    ],
    'product70' => [
        'title' => 'voluptas exercitationem',
        'description' => 'Quibusdam ut dolore aut est ex accusamus.',
    ],
    'product71' => [
        'title' => 'qui corrupti',
        'description' => 'Cum tenetur quia omnis inventore et officia.',
    ],
    'product72' => [
        'title' => 'quod quia',
        'description' => 'Explicabo ut tempore doloribus dolores.',
    ],
    'product73' => [
        'title' => 'ad dicta',
        'description' => 'Qui tempora magnam et est eveniet ullam sunt.',
    ],
    'product74' => [
        'title' => 'doloribus beatae',
        'description' => 'Qui voluptatem error quod aut.',
    ],
    'product75' => [
        'title' => 'eligendi porro',
        'description' => 'Ex earum quae veritatis quis commodi aliquam.',
    ],
    'product76' => [
        'title' => 'incidunt quia',
        'description' => 'Non quia illum eligendi rerum quia.',
    ],
    'product77' => [
        'title' => 'omnis sint',
        'description' => 'Quidem dolore et aliquam illo.',
    ],
    'product78' => [
        'title' => 'quia voluptas',
        'description' => 'Nihil iure non consequatur.',
    ],
    'product79' => [
        'title' => 'aspernatur eum',
        'description' => 'Sed rerum nobis tenetur in occaecati.',
    ],
    'product80' => [
        'title' => 'praesentium beatae',
        'description' => 'Neque quibusdam voluptatem itaque aut nam vitae.',
    ],
    'product81' => [
        'title' => 'aut vel',
        'description' => 'Accusamus voluptatem doloribus cum quibusdam aut.',
    ],
    'product82' => [
        'title' => 'sit omnis',
        'description' => 'Necessitatibus id nihil ea dolorem.',
    ],
    'product83' => [
        'title' => 'sit unde',
        'description' => 'Enim vero quod soluta.',
    ],
    'product84' => [
        'title' => 'voluptas totam',
        'description' => 'Culpa commodi hic corrupti sapiente hic dicta.',
    ],
    'product85' => [
        'title' => 'quidem sed',
        'description' => 'Laboriosam ut earum sint magnam.',
    ],
    'product86' => [
        'title' => 'deleniti voluptates',
        'description' => 'Earum nisi qui error eum.',
    ],
    'product87' => [
        'title' => 'facere nihil',
        'description' => 'Quia at cumque omnis iure qui id voluptates.',
    ],
    'product88' => [
        'title' => 'et in',
        'description' => 'Soluta eos et modi voluptatem.',
    ],
    'product89' => [
        'title' => 'ea ipsum',
        'description' => 'Beatae deserunt numquam enim quo ut.',
    ],
    'product90' => [
        'title' => 'et et',
        'description' => 'Et consequuntur sit illo minima.',
    ],
    'product91' => [
        'title' => 'rem dolor',
        'description' => 'Maxime occaecati ducimus ea.',
    ],
    'product92' => [
        'title' => 'tempore et',
        'description' => 'Molestias facilis laborum rerum veniam.',
    ],
    'product93' => [
        'title' => 'vero ullam',
        'description' => 'Repudiandae earum dolor laborum reiciendis.',
    ],
    'product94' => [
        'title' => 'aspernatur modi',
        'description' => 'Ea hic soluta odit ut assumenda aut.',
    ],
    'product95' => [
        'title' => 'omnis aliquid',
        'description' => 'Qui nulla omnis vero.',
    ],
    'product96' => [
        'title' => 'voluptas unde',
        'description' => 'Sunt eligendi voluptatibus ut enim sit.',
    ],
    'product97' => [
        'title' => 'dicta modi',
        'description' => 'Quae illo voluptas velit et vitae.',
    ],
    'product98' => [
        'title' => 'facilis et',
        'description' => 'Quia rerum eum rerum omnis.',
    ],
    'product99' => [
        'title' => 'placeat nesciunt',
        'description' => 'Vero ipsa at et. Aut blanditiis excepturi fugiat.',
    ],
];
