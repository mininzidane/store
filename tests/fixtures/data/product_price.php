<?php

return [
    'product_price0' => [
        'price' => 103743.06200000001,
        'product_id' => 1,
    ],
    'product_price1' => [
        'price' => 864980.34999999998,
        'product_id' => 2,
    ],
    'product_price2' => [
        'price' => 903751.94499999995,
        'product_id' => 3,
    ],
    'product_price3' => [
        'price' => 601550.45200000005,
        'product_id' => 4,
    ],
    'product_price4' => [
        'price' => 428103.35200000001,
        'product_id' => 5,
    ],
    'product_price5' => [
        'price' => 913372.30299999996,
        'product_id' => 6,
    ],
    'product_price6' => [
        'price' => 207657.51800000001,
        'product_id' => 7,
    ],
    'product_price7' => [
        'price' => 386985.13500000001,
        'product_id' => 8,
    ],
    'product_price8' => [
        'price' => 685847.61100000003,
        'product_id' => 9,
    ],
    'product_price9' => [
        'price' => 444442.00799999997,
        'product_id' => 10,
    ],
    'product_price10' => [
        'price' => 295646.29599999997,
        'product_id' => 11,
    ],
    'product_price11' => [
        'price' => 624566.17200000002,
        'product_id' => 12,
    ],
    'product_price12' => [
        'price' => 819747.95900000003,
        'product_id' => 13,
    ],
    'product_price13' => [
        'price' => 531633.38899999997,
        'product_id' => 14,
    ],
    'product_price14' => [
        'price' => 5173.9690000000001,
        'product_id' => 15,
    ],
    'product_price15' => [
        'price' => 760143.60800000001,
        'product_id' => 16,
    ],
    'product_price16' => [
        'price' => 15365.243,
        'product_id' => 17,
    ],
    'product_price17' => [
        'price' => 309880.46100000001,
        'product_id' => 18,
    ],
    'product_price18' => [
        'price' => 560117.58200000005,
        'product_id' => 19,
    ],
    'product_price19' => [
        'price' => 385348.06,
        'product_id' => 20,
    ],
    'product_price20' => [
        'price' => 389513.14299999998,
        'product_id' => 21,
    ],
    'product_price21' => [
        'price' => 109052.181,
        'product_id' => 22,
    ],
    'product_price22' => [
        'price' => 605651.06299999997,
        'product_id' => 23,
    ],
    'product_price23' => [
        'price' => 451740.92800000001,
        'product_id' => 24,
    ],
    'product_price24' => [
        'price' => 240068.236,
        'product_id' => 25,
    ],
    'product_price25' => [
        'price' => 537314.06499999994,
        'product_id' => 26,
    ],
    'product_price26' => [
        'price' => 226215.15700000001,
        'product_id' => 27,
    ],
    'product_price27' => [
        'price' => 924950.41700000002,
        'product_id' => 28,
    ],
    'product_price28' => [
        'price' => 711224.33900000004,
        'product_id' => 29,
    ],
    'product_price29' => [
        'price' => 335543.06199999998,
        'product_id' => 30,
    ],
    'product_price30' => [
        'price' => 736744.84499999997,
        'product_id' => 31,
    ],
    'product_price31' => [
        'price' => 232120.39000000001,
        'product_id' => 32,
    ],
    'product_price32' => [
        'price' => 791720.152,
        'product_id' => 33,
    ],
    'product_price33' => [
        'price' => 206367.81299999999,
        'product_id' => 34,
    ],
    'product_price34' => [
        'price' => 752146.68900000001,
        'product_id' => 35,
    ],
    'product_price35' => [
        'price' => 824240.20499999996,
        'product_id' => 36,
    ],
    'product_price36' => [
        'price' => 149035.99299999999,
        'product_id' => 37,
    ],
    'product_price37' => [
        'price' => 617349.70999999996,
        'product_id' => 38,
    ],
    'product_price38' => [
        'price' => 380405.076,
        'product_id' => 39,
    ],
    'product_price39' => [
        'price' => 300997.076,
        'product_id' => 40,
    ],
    'product_price40' => [
        'price' => 996221.79099999997,
        'product_id' => 41,
    ],
    'product_price41' => [
        'price' => 884.40099999999995,
        'product_id' => 42,
    ],
    'product_price42' => [
        'price' => 924524.15599999996,
        'product_id' => 43,
    ],
    'product_price43' => [
        'price' => 903971.68500000006,
        'product_id' => 44,
    ],
    'product_price44' => [
        'price' => 757135.73300000001,
        'product_id' => 45,
    ],
    'product_price45' => [
        'price' => 623291.30900000001,
        'product_id' => 46,
    ],
    'product_price46' => [
        'price' => 522506.38799999998,
        'product_id' => 47,
    ],
    'product_price47' => [
        'price' => 664751.83299999998,
        'product_id' => 48,
    ],
    'product_price48' => [
        'price' => 477166.65299999999,
        'product_id' => 49,
    ],
    'product_price49' => [
        'price' => 282267.00199999998,
        'product_id' => 50,
    ],
    'product_price50' => [
        'price' => 833877.21400000004,
        'product_id' => 51,
    ],
    'product_price51' => [
        'price' => 258551.38099999999,
        'product_id' => 52,
    ],
    'product_price52' => [
        'price' => 240683.79000000001,
        'product_id' => 53,
    ],
    'product_price53' => [
        'price' => 204227.166,
        'product_id' => 54,
    ],
    'product_price54' => [
        'price' => 593367.973,
        'product_id' => 55,
    ],
    'product_price55' => [
        'price' => 324637.02399999998,
        'product_id' => 56,
    ],
    'product_price56' => [
        'price' => 936191.83499999996,
        'product_id' => 57,
    ],
    'product_price57' => [
        'price' => 634999.65800000005,
        'product_id' => 58,
    ],
    'product_price58' => [
        'price' => 489564.46600000001,
        'product_id' => 59,
    ],
    'product_price59' => [
        'price' => 273825.728,
        'product_id' => 60,
    ],
    'product_price60' => [
        'price' => 595910.81099999999,
        'product_id' => 61,
    ],
    'product_price61' => [
        'price' => 740585.22100000002,
        'product_id' => 62,
    ],
    'product_price62' => [
        'price' => 175529.83100000001,
        'product_id' => 63,
    ],
    'product_price63' => [
        'price' => 441754.05800000002,
        'product_id' => 64,
    ],
    'product_price64' => [
        'price' => 518592.72999999998,
        'product_id' => 65,
    ],
    'product_price65' => [
        'price' => 860842.62699999998,
        'product_id' => 66,
    ],
    'product_price66' => [
        'price' => 341792.63799999998,
        'product_id' => 67,
    ],
    'product_price67' => [
        'price' => 871131.76599999995,
        'product_id' => 68,
    ],
    'product_price68' => [
        'price' => 430120.027,
        'product_id' => 69,
    ],
    'product_price69' => [
        'price' => 155892.85399999999,
        'product_id' => 70,
    ],
    'product_price70' => [
        'price' => 521796.13900000002,
        'product_id' => 71,
    ],
    'product_price71' => [
        'price' => 370626.68400000001,
        'product_id' => 72,
    ],
    'product_price72' => [
        'price' => 262120.61799999999,
        'product_id' => 73,
    ],
    'product_price73' => [
        'price' => 727930.04200000002,
        'product_id' => 74,
    ],
    'product_price74' => [
        'price' => 994008.93099999998,
        'product_id' => 75,
    ],
    'product_price75' => [
        'price' => 479222.44400000002,
        'product_id' => 76,
    ],
    'product_price76' => [
        'price' => 184531.51699999999,
        'product_id' => 77,
    ],
    'product_price77' => [
        'price' => 519538.946,
        'product_id' => 78,
    ],
    'product_price78' => [
        'price' => 463124.12800000003,
        'product_id' => 79,
    ],
    'product_price79' => [
        'price' => 446285.06599999999,
        'product_id' => 80,
    ],
    'product_price80' => [
        'price' => 82263.399000000005,
        'product_id' => 81,
    ],
    'product_price81' => [
        'price' => 527251.66899999999,
        'product_id' => 82,
    ],
    'product_price82' => [
        'price' => 751898.12,
        'product_id' => 83,
    ],
    'product_price83' => [
        'price' => 405112.91499999998,
        'product_id' => 84,
    ],
    'product_price84' => [
        'price' => 883342.24600000004,
        'product_id' => 85,
    ],
    'product_price85' => [
        'price' => 434186.20799999998,
        'product_id' => 86,
    ],
    'product_price86' => [
        'price' => 190163.62299999999,
        'product_id' => 87,
    ],
    'product_price87' => [
        'price' => 312022.83899999998,
        'product_id' => 88,
    ],
    'product_price88' => [
        'price' => 556160.799,
        'product_id' => 89,
    ],
    'product_price89' => [
        'price' => 422704.12800000003,
        'product_id' => 90,
    ],
    'product_price90' => [
        'price' => 343696.94699999999,
        'product_id' => 91,
    ],
    'product_price91' => [
        'price' => 974479.47199999995,
        'product_id' => 92,
    ],
    'product_price92' => [
        'price' => 842770.33299999998,
        'product_id' => 93,
    ],
    'product_price93' => [
        'price' => 48919.192999999999,
        'product_id' => 94,
    ],
    'product_price94' => [
        'price' => 249362.60699999999,
        'product_id' => 95,
    ],
    'product_price95' => [
        'price' => 990135.946,
        'product_id' => 96,
    ],
    'product_price96' => [
        'price' => 62735.436000000002,
        'product_id' => 97,
    ],
    'product_price97' => [
        'price' => 965362.85699999996,
        'product_id' => 98,
    ],
    'product_price98' => [
        'price' => 169684.75,
        'product_id' => 99,
    ],
    'product_price99' => [
        'price' => 662070.12100000004,
        'product_id' => 100,
    ],
];
