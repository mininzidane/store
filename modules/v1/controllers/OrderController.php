<?php

namespace app\modules\v1\controllers;

use app\components\Cart;
use app\models\Order;
use yii\web\NotFoundHttpException;

class OrderController extends ApiController
{
    public function actionPreCreate(): array
    {
        $cartDataProducts = \Yii::$app->request->post('product_ids', []);
        $listIds = \Yii::$app->request->post('list_ids', []);
        /** @var Cart $cart */
        $cart = \Yii::$container->get(Cart::class);

        return $this->getResponse(true, [
            'products' => $cart->getProductInfoFromCartData($cart->analyzeProductsAndLists($cartDataProducts, $listIds))
        ]);
    }

    public function actionCreate(): array
    {
        $cartData = \Yii::$app->request->post('product_ids', []);
        /** @var Cart $cart */
        $cart = \Yii::$container->get(Cart::class);
        [
            'success' => $success,
            'orderId' => $orderId,
            'products' => $products,
        ] = $cart->create($cartData);

        return $this->getResponse($success, [
            'order' => [
                'id' => $orderId,
                'products' => $products,
            ]
        ]);
    }

    public function actionList(): array
    {
        $orders = Order::find()
            ->own()
            ->active()
            ->limited()
            ->all();

        return $this->getResponse(true, ['orders' => $orders]);
    }

    public function actionDelete(int $id): array
    {
        $order = Order::findOne($id);
        if ($order === null) {
            throw new NotFoundHttpException("Could not find order #{$id}");
        }

        $order->status = Order::STATUS_INACTIVE;
        if (!$order->save(false, ['status'])) {
            return $this->getResponse(false, [
                'error' => "Could not delete order #{$id}"
            ]);
        }

        return $this->getResponse();
    }
}
