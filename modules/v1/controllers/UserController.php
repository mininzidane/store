<?php

namespace app\modules\v1\controllers;

use app\models\LoginForm;
use app\models\User;

class UserController extends ApiController
{
    public function actionCreate(): array
    {
        $username = \Yii::$app->request->post('username');
        $password = \Yii::$app->request->post('password');
        $user = new User();
        $user->username = $username;
        $user->password = $password;
        $user->access_token = \Yii::$app->security->generateRandomString();
        if (!$user->save()) {
            return $this->getResponse(false, [
                'errors' => $user->errors
            ]);
        }

        return $this->getResponse(true, [
            'token' => $user->access_token,
        ]);
    }

    public function actionLogin(): array
    {
        $username = \Yii::$app->request->post('username');
        $password = \Yii::$app->request->post('password');
        $rememberMe = false;
        $loginForm = new LoginForm();
        $loginForm->attributes = compact('username', 'password', 'rememberMe');

        if (!$loginForm->login()) {
            return $this->getResponse(false, [
                'errors' => $loginForm->errors,
            ]);
        }

        return $this->getResponse(true, [
            'token' => $loginForm->getUser()->access_token,
        ]);
    }
}
