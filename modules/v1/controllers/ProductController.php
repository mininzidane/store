<?php

namespace app\modules\v1\controllers;

use app\models\Product;
use app\models\ProductToCategory;

class ProductController extends ApiController
{
    public function actionSearchByCategories($ids): array
    {
        $ids = explode(',', $ids);
        if (empty($ids)) {
            $products = [];
        } else {
            $products = Product::find()
                ->innerJoinWith('productToCategories')
                ->where([ProductToCategory::tableName() . '.category_id' => $ids])
                ->limited()
                ->orderBy(ProductToCategory::tableName() . '.category_id ASC')
                ->all()
            ;
        }

        return $this->getResponse(true, compact('products'));
    }
}
