<?php

namespace app\modules\v1\controllers;

use app\modules\v1\Module;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\web\Response;

class ApiController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors(): array
    {
        $config = [
            'contentNegotiator' => [
                'class' => ContentNegotiator::class,
                'formatParam' => '_format',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
        if (!YII_DEBUG) {
            $config['authenticator'] = [
                'class' => CompositeAuth::class,
                'authMethods' => [
                    ['class' => HttpBearerAuth::class],
                    [
                        'class' => QueryParamAuth::class,
                        'tokenParam' => 'accessToken',
                    ],
                ],
            ];
        }
        return array_merge(
            parent::behaviors(),
            $config
        );
    }

    protected function getResponse(bool $success = true, array $data = []): array
    {
        return Module::getResponse($success, $data);
    }
}
