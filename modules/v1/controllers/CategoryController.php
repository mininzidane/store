<?php

namespace app\modules\v1\controllers;

use app\models\Category;
use yii\db\Query;

class CategoryController extends ApiController
{
    public function actionSearch(string $query = ''): array
    {
        $categories = Category::find()
            ->where([
                'LIKE', 'title', $query,
            ])
            ->all();

        return $this->getResponse(true, [
            'categories' => $categories
        ]);
    }

    public function actionList($q = null, $id = null): array
    {
        $data = [];
        if (is_numeric($id)) {
            $category = Category::findOne($id);

            $data['items'] = $category
                ? [
                    'id' => $id,
                    'text' => $category->title,
                ]
                : [];
        } else {
            $query = new Query();
            $query->select('id, title AS text')
                ->from(Category::tableName())
                ->limit(20);
            if ($q !== null) {
                $query->andWhere([
                    'LIKE',
                    'title',
                    $q,
                ]);
            }

            $data['items'] = array_values($query->createCommand()->queryAll());
        }
        return $data;
    }
}
