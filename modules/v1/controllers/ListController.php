<?php

namespace app\modules\v1\controllers;

use app\components\Cart;
use app\models\ProductList;
use yii\web\NotFoundHttpException;

class ListController extends ApiController
{
    private function updateModel(ProductList $productList): array
    {
        $cartDataProducts = \Yii::$app->request->post('product_ids', []);
        $productList->title = \Yii::$app->request->post('title');
        $productList->setCardData($cartDataProducts);
        /** @var Cart $cart */
        $cart = \Yii::$container->get(Cart::class);
        if ($productList->save()) {
            return $this->getResponse(true, [
                'list' => [
                    'id' => $productList->id,
                    'title' => $productList->title,
                    'products' => $cart->getProductInfoFromCartData($cartDataProducts),
                ]
            ]);
        }

        return $this->getResponse(false, ['errors' => $productList->errors]);
    }

    public function actionCreate(): array
    {
        $productList = new ProductList();
        return $this->updateModel($productList);
    }

    public function actionUpdate(int $id): array
    {
        $productList = ProductList::findOne($id);
        if ($productList === null) {
            throw new NotFoundHttpException("Could not find product list #{$id}");
        }
        return $this->updateModel($productList);
    }

    public function actionList(): array
    {
        $list = ProductList::find()
            ->own()
            ->active()
            ->limited()
            ->all();

        return $this->getResponse(true, ['productLists' => $list]);
    }

    public function actionDelete(int $id): array
    {
        $productList = ProductList::findOne($id);
        if ($productList === null) {
            throw new NotFoundHttpException("Could not find product list #{$id}");
        }

        // todo may be no need to delete it - set status inactive instead
        if (!$productList->delete()) {
            return $this->getResponse(false, [
                'error' => "Could not delete product list #{$id}"
            ]);
        }

        return $this->getResponse();
    }
}