<?php

namespace app\modules\v1;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    private const RESPONSE_STATUS_OK = 'ok';
    private const RESPONSE_STATUS_NOT_OK = 'error';

    public static function getResponse(bool $success, array $data = []): array
    {
        return [
            'status' => $success ? self::RESPONSE_STATUS_OK : self::RESPONSE_STATUS_NOT_OK,
            'data' => $data,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\v1\controllers';

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();
    }
}
