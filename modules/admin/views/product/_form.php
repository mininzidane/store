<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */

$resultsJs = <<<JS
function(data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'categoryIds')->widget(\kartik\select2\Select2::class, [
        'language' => 'ru',
        'data' => empty($model->categoryIds) ? [] : \app\models\Category::getList($model->categoryIds),
        'options' => [
            'placeholder' => 'Select a category',
            'multiple'    => true,
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/category/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q: params.q, page: params.page}; }'),
                'processResults' => new JsExpression($resultsJs),
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
