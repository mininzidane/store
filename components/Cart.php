<?php

namespace app\components;

use app\models\Order;
use app\models\Product;
use app\models\ProductList;

class Cart
{
    /**
     * @param array $cartData [productId => quantity, ...]
     * @return array
     */
    public function create(array $cartData): array
    {
        $order = new Order();
        $order->user_id = \Yii::$app->user->id;
        $order->setCardData($cartData);
        $success = $order->save();
        return [
            'success' => $success,
            'orderId' => $order->id,
            'products' => $this->getProductInfoFromCartData($cartData),
        ];
    }

    /**
     * @param array $cartDataProducts [productId => quantity, ...]
     * @param array $listIds [1, 2, 3 ...]
     * @return array [productId => quantity, ...]
     */
    public function analyzeProductsAndLists(array $cartDataProducts, array $listIds): array
    {
        $cartDataOutput = [];
        $lists = ProductList::find()
            ->with('productListToProducts', 'productListToProducts.product')
            ->where([
                'id' => $listIds,
                'user_id' => \Yii::$app->user->id,
            ])
            ->all();

        foreach ($lists as $productList) {
            foreach ($productList->productListToProducts as $productListToProduct) {
                $cartDataOutput[$productListToProduct->product_id] = $productListToProduct->quantity;
            }
        }

        return array_replace($cartDataOutput, $cartDataProducts);
    }

    /**
     * @param array $cartData [productId => quantity, ...]
     * @return array
     */
    public function getProductInfoFromCartData(array $cartData): array
    {
        $products = Product::find()
            ->where(['id' => array_keys($cartData)])
            ->all();

        $output = [];
        foreach ($products as $product) {
            $output[] = [
                'quantity' => $cartData[$product->id],
                'product' => $product->toArray(),
            ];
        }
        return $output;
    }
}
